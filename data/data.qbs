Product {
    name: "Data";
    qbs.install: true

    // FIXME: Should be done in simpletexteditor QBS code
    Group {
        name: "Samples"
        qbs.install: true
        qbs.installDir: project.leda_data_path + "/samples"
        files: [
            "samples/helloworld.txt",
        ]
    }

    // FIXME: Should be done in footprinteditor QBS code
    Group {
        name: "Footprint Editor Samples"
        qbs.install: true
        qbs.installDir: project.leda_data_path + "/samples/footprints"
        files: [
            "samples/footprint/example1.LeFoot",
        ]
    }

    // FIXME: Should be done in symboleditor QBS code
    Group {
        name: "Symbol Editor Samples"
        qbs.install: true
        qbs.installDir: project.leda_data_path + "/samples/symbols"
        files: [
            "samples/sch/example1.LeSch"
        ]
    }

    // FIXME: Should be done in symboleditor QBS code
    Group {
        name: "Gerber Editor Samples"
        qbs.install: true
        qbs.installDir: project.leda_data_path + "/samples/gerbers"
        files: [
            "samples/gerber/am-1.gbr",
            "samples/gerber/panellization-B_Cu.gbl",
            "samples/gerber/panellization-B_Mask.gbs",
            "samples/gerber/panellization-B_SilkS.gbo",
            "samples/gerber/panellization-Edge_Cuts.gbr",
            "samples/gerber/panellization-F_Cu.gtl",
            "samples/gerber/panellization-F_Mask.gts",
            "samples/gerber/panellization-F_SilkS.gto",

        ]
    }

    // FIXME: Should be done in symboleditor QBS code
    Group {
        name: "Symbol Editor Settings"
        qbs.install: true
        qbs.installDir: project.leda_data_path + "/settings/symboleditor"
        files: [
            "settings/sch/solarized-light.lesympalette",
            "settings/sch/solarized-dark.lesympalette",
        ]
    }

    Group {
        name: "IPC7351 samples"
        qbs.install: true
        qbs.installDir: project.leda_data_path + "/samples/IPC-7351"
        files: [
            "samples/ipc7351/test2.xml",
            "samples/ipc7351/test3.xml",
            "samples/ipc7351/test4.xml",
            "samples/ipc7351/test5.xml",
            "samples/ipc7351/test.xml",
        ]
    }

    Group {
        name: "IPC2581 samples"
        qbs.install: true
        qbs.installDir: project.leda_data_path + "/samples/IPC-2581"
        files: [
            "samples/ipc2581/12_Layer.xml",
            "samples/ipc2581/ICD-stackup-planner-8L-1.xml",
            "samples/ipc2581/test-3_r2.xml",
            "samples/ipc2581/testcase2-stackup-only.xml",
            "samples/ipc2581/testcase2.xml",
            "samples/ipc2581/testcase5-stackup-only.xml",
            "samples/ipc2581/testcase5.xml",
            "samples/ipc2581/testcase6-stackup-only.xml",
            "samples/ipc2581/testcase6.xml",
        ]
    }
}
