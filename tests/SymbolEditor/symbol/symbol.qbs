import qbs

LedaAutotest {
    name: "Autotests.SymbolEditor.XdlSymbol"

    Depends { name: "Xdl" }
    Depends { name: "Qt"; submodules: ["core", "gui"] }

    files: [
        "tst_symbol.cpp",
    ]
}
