#ifndef TESTDOCUMENTOBJECTLISTENER_H
#define TESTDOCUMENTOBJECTLISTENER_H

#include <QObject>

#include "LeDocumentObject/IDocumentObjectListener.h"

class TestDocumentObjectListener : public QObject, public LDO::IDocumentObjectListener
{
    Q_OBJECT
public:
    explicit TestDocumentObjectListener(QObject *parent = 0);

private slots:
    void init();
    void cleanup();

    void testInsertChildObject();
    void testRemoveChildObject();
    void testChangeStaticProperty();
    void testChangeDynamicProperty();

    void testCannotListenTwice();
    void testCannotInsertTwice();
    void testCannotRemoveTwice();

private:
    void clear();

    QString firstCalled;

    int objectAboutToBeInsertedCallCount;
    const LDO::IDocumentObject *objectAboutToBeInsertedParent;
    const LDO::IDocumentObject *objectAboutToBeInsertedChild;
    int objectAboutToBeInsertedIndex;

    int objectInsertedCallCount;
    const LDO::IDocumentObject *objectInsertedParent;
    const LDO::IDocumentObject *objectInsertedChild;
    int objectInsertedIndex;

    int objectAboutToBeRemovedCallCount;
    const LDO::IDocumentObject *objectAboutToBeRemovedParent;
    const LDO::IDocumentObject *objectAboutToBeRemovedChild;
    int objectAboutToBeRemovedIndex;

    int objectRemovedCallCount;
    const LDO::IDocumentObject *objectRemovedParent;
    const LDO::IDocumentObject *objectRemovedChild;
    int objectRemovedIndex;

    int objectPropertyAboutToChangeCallCount;
    const LDO::IDocumentObject *objectPropertyAboutToChangeObject;
    QString objectPropertyAboutToChangeProperty;
    QVariant objectPropertyAboutToChangeValue;

    int objectPropertyChangedCallCount;
    const LDO::IDocumentObject *objectPropertyChangedObject;
    QString objectPropertyChangedProperty;
    QVariant objectPropertyChangedValue;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToBeInserted(LDO::IDocumentObject *parent,
                                                 LDO::IDocumentObject *child,
                                                 int index) override;
    virtual void documentObjectInserted(LDO::IDocumentObject *parent,
                                        LDO::IDocumentObject *child,
                                        int index) override;
    virtual void documentObjectAboutToBeRemoved(LDO::IDocumentObject *parent,
                                                LDO::IDocumentObject *child,
                                                int index) override;
    virtual void documentObjectRemoved(LDO::IDocumentObject *parent,
                                       LDO::IDocumentObject *child,
                                       int index) override;
    virtual void documentObjectAboutToChangeProperty(const LDO::IDocumentObject *object,
                                                     const QString &name,
                                                     const QVariant &value) override;
    virtual void documentObjectPropertyChanged(const LDO::IDocumentObject *object,
                                               const QString &name,
                                               const QVariant &value) override;
};

#endif // TESTDOCUMENTOBJECTLISTENER_H
