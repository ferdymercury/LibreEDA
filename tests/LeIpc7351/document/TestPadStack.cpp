#include "TestPadStack.h"

#include "LeIpc7351/PadStack.h"
#include "LeIpc7351/PadStackElement.h"
#include "LeIpc7351/Primitives/CirclePrimitive.h"

#include <QtTest>

TestPadStack::TestPadStack(QObject *parent) : QObject(parent)
{

}

void TestPadStack::testCreatePadStackElement()
{
    PadStackElement *element = new PadStackElement;
    element->setFunction(L7::PadStackElementFunction::InternalLand);
    element->setPrimitive(new CirclePrimitive);
}

void TestPadStack::testCreatePadStack()
{
    PadStack *stack = new PadStack;
    stack->setRole(L7::PadStackRole::SurfaceTerminal);
    QList<PadStackElement*> elements;
    elements << createElement();
    elements << createElement();
    stack->setElements(elements);
}

PadStackElement *TestPadStack::createElement()
{
    PadStackElement *element = new PadStackElement;
    element->setFunction(L7::PadStackElementFunction::InternalLand);
    element->setPrimitive(new CirclePrimitive);
    return element;
}
