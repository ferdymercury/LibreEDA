import qbs

Project {
    name: "Applications"
    references: [
        "LibreEDA",
        "LeGerberViewer",
        "LeGerberParser",
        "LeIpc2581Viewer",
        "LePackageDesigner"
    ]
}
