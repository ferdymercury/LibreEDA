#pragma once

#include "qtpropertybrowser/qtvariantproperty.h"
#include "LeIpc7351/LeIpc7351.h"

class PropertyManager: public QtVariantPropertyManager
{
    Q_OBJECT

public:
    explicit PropertyManager(QObject *parent = nullptr);
    ~PropertyManager();

    static int transformTypeId();

private slots:
    void slotValueChanged(QtProperty *property, const QVariant &value);
    void slotPropertyDestroyed(QtProperty *property);

private:
    struct TransformData
    {
        QVariant value;
        QtVariantProperty *rotation;
        QtVariantProperty *mirror;
    };
    QMap<const QtProperty*, TransformData> m_propertyTotransformData;
    QMap<const QtProperty*, QtProperty*> m_transformRotationToProperty;
    QMap<const QtProperty*, QtProperty*> m_transformMirrorToProperty;

    // QtAbstractPropertyManager interface
protected:
    virtual QString valueText(const QtProperty *property) const override;
    virtual void initializeProperty(QtProperty *property) override;
    virtual void uninitializeProperty(QtProperty *property) override;

    // QtVariantPropertyManager interface
public:
    virtual bool isPropertyTypeSupported(int propertyType) const override;
    virtual int valueType(int propertyType) const override;
    virtual QVariant value(const QtProperty *property) const override;

public slots:
    virtual void setValue(QtProperty *property, const QVariant &value) override;
};

