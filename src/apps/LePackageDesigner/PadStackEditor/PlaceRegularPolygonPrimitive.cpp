#include "PlaceRegularPolygonPrimitive.h"

#include "PadStackWidget.h"
#include "Gui.h"
#include "GraphicsRegularPolygonPrimitive.h"

#include "LeIpc7351/PadStackElement.h"
#include "LeIpc7351/Primitives/RegularPolygonPrimitive.h"

#include "LeGraphicsView/LeGraphicsItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"
#include "LeGraphicsView/LeGraphicsStyle.h"
#include "LeGraphicsView/LeGraphicsPalette.h"

#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QRectF>

#include <cmath>

PlaceRegularPolygonPrimitiveTask::PlaceRegularPolygonPrimitiveTask(PadStackWidget *editor)
    : PadStackTask(editor)
    , LeGraphicsSceneEventFilter()
{
    setTile("Regular polygon pad");
    setDescription("Place a regular polygon pad");
    setIcon(Gui::icon("primitive-shape-regular-polygon"));
    setShortcut(QKeySequence("p,g"));
}


void PlaceRegularPolygonPrimitiveTask::start()
{
    m_primitive = new RegularPolygonPrimitive();
    m_item = new GraphicsRegularPolygonPrimitive(m_primitive);
    m_state = WaitForP1;
    scene()->setEventFilter(this);
    emit started(this);
}

void PlaceRegularPolygonPrimitiveTask::accept()
{
    Q_ASSERT(m_state == Finished);

    scene()->currentLayer()->removeFromLayer(m_item);
    delete m_item;
    m_item = nullptr;
    editor()->currentElement()->setPrimitive(m_primitive); // FIXME
    scene()->setEventFilter(nullptr);
    emit accepted(this);
}

void PlaceRegularPolygonPrimitiveTask::reject()
{
    if (m_state != WaitForP1)
    {
        scene()->currentLayer()->removeFromLayer(m_item);
    }
    delete m_item;
    m_item = nullptr;
    delete m_primitive;
    m_primitive = nullptr;
    scene()->setEventFilter(nullptr);
    emit rejected(this);
}

void PlaceRegularPolygonPrimitiveTask::setSideCount(int sideCount)
{
    if (m_primitive->sideCount() == sideCount)
        return;

    m_primitive->setSideCount(sideCount);
    emit sideCountChanged(m_primitive->sideCount());
}

void PlaceRegularPolygonPrimitiveTask::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(painter)
    Q_UNUSED(rect)
}

void PlaceRegularPolygonPrimitiveTask::drawForeground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect)

    if (m_item == nullptr)
        return;

    painter->setBrush(Qt::NoBrush);
    painter->setPen(QPen(scene()->graphicsPalette().color(LeGraphicsPalette::Highlight), 0));
    painter->drawLine(m_radiusLine);
    painter->setPen(QPen(scene()->graphicsPalette().color(LeGraphicsPalette::Highlight), 0, Qt::DotLine));
    QRectF bRect = m_item->boundingRect();
    bRect.moveCenter(m_item->pos());
    painter->drawRect(bRect);
}

bool PlaceRegularPolygonPrimitiveTask::keyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceRegularPolygonPrimitiveTask::keyReleaseEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceRegularPolygonPrimitiveTask::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceRegularPolygonPrimitiveTask::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_state == WaitForP2)
    {
        m_radiusLine.setP2(event->scenePos());
        m_primitive->setDiameter(qMax(qAbs(m_radiusLine.dx()),
                                      qAbs(m_radiusLine.dy()))*2.0);
        m_primitive->setRotation(std::fmod(2*360-m_radiusLine.angle()-90, 360.0));
        qDebug() << m_radiusLine.angle() << m_primitive->rotation();
    }
    return true;
}

bool PlaceRegularPolygonPrimitiveTask::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_state == WaitForP1)
    {
        m_primitive->setPosition(event->scenePos());
        scene()->currentLayer()->addToLayer(m_item);
        m_radiusLine.setP1(event->scenePos());
        m_state = WaitForP2;
        return true;
    }
    else if (m_state == WaitForP2)
    {
        m_state = Finished;
        accept();
    }
    return true;
}

bool PlaceRegularPolygonPrimitiveTask::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    return true;
}

int PlaceRegularPolygonPrimitiveTask::sideCount() const
{
    return m_primitive->sideCount();
}
