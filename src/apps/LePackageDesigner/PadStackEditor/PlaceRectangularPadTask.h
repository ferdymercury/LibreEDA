#pragma once

#include "PadStackTask.h"

#include "LeGraphicsView/LeGraphicsSceneEventFilter.h"

#include <QPointF>
#include <QLineF>

class PadStackElement;

class LeGraphicsRectItem;

class PlaceRectangularPadTask: public PadStackTask, public LeGraphicsSceneEventFilter
{
    Q_OBJECT

    Q_PROPERTY(QPointF position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)

public:
    explicit PlaceRectangularPadTask(PadStackWidget *editor = nullptr);

    QPointF position() const;
    QSizeF size() const;

signals:
    void positionChanged(QPointF position);
    void sizeChanged(const QSizeF &size);

public slots:
    void setPosition(QPointF position);
    void setSize(const QSizeF &size);

private:
    QPointF m_position;
    QSizeF m_size;
    PadStackElement *m_element = nullptr;
    LeGraphicsRectItem *m_item = nullptr;
    QLineF m_radiusLine;

    // AbstractTask interface
public slots:
    virtual void start() override;
    virtual void accept() override;
    virtual void reject() override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual void drawBackground(QPainter *painter, const QRectF &rect) override;
    virtual void drawForeground(QPainter *painter, const QRectF &rect) override;
    virtual bool keyPressEvent(QKeyEvent *event) override;
    virtual bool keyReleaseEvent(QKeyEvent *event) override;
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
};
