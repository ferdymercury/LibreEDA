#ifndef SMDPADCALCULATORWIDGET_H
#define SMDPADCALCULATORWIDGET_H

#include <QWidget>

class QDoubleSpinBox;
namespace Ui {
    class SmdPadCalculatorWidget;
}

class SmdPadCalculator;
class SmdPadCalculatorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SmdPadCalculatorWidget(QWidget *parent = 0);
    ~SmdPadCalculatorWidget();

private:
    Ui::SmdPadCalculatorWidget *m_ui;
    SmdPadCalculator *m_calculator;
    void bind(const QString &name, QDoubleSpinBox *spinBox, qreal value);
    void showResult();
};

#endif // SMDPADCALCULATORWIDGET_H
