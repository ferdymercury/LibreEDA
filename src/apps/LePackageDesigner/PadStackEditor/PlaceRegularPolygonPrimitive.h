#pragma once

#include "PadStackTask.h"

#include "LeGraphicsView/LeGraphicsSceneEventFilter.h"

#include <QPointF>
#include <QLineF>

class RegularPolygonPrimitive;
class GraphicsRegularPolygonPrimitive;

class PlaceRegularPolygonPrimitiveTask: public PadStackTask, public LeGraphicsSceneEventFilter
{
    Q_OBJECT

    Q_PROPERTY(int sideCount READ sideCount WRITE setSideCount NOTIFY sideCountChanged)

public:
    explicit PlaceRegularPolygonPrimitiveTask(PadStackWidget *editor = nullptr);

    int sideCount() const;

signals:
    void sideCountChanged(int sideCount);

public slots:
    void setSideCount(int sideCount);

private:
    enum
    {
        WaitForP1,
        WaitForP2,
        Finished
    };
    int m_state = WaitForP1;
    RegularPolygonPrimitive *m_primitive = nullptr;
    GraphicsRegularPolygonPrimitive *m_item = nullptr;
    QLineF m_radiusLine;

    // AbstractTask interface
public slots:
    virtual void start() override;
    virtual void accept() override;
    virtual void reject() override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual void drawBackground(QPainter *painter, const QRectF &rect) override;
    virtual void drawForeground(QPainter *painter, const QRectF &rect) override;
    virtual bool keyPressEvent(QKeyEvent *event) override;
    virtual bool keyReleaseEvent(QKeyEvent *event) override;
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
};
