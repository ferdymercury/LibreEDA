#ifndef SMDPADCALCULATOR_H
#define SMDPADCALCULATOR_H

#include <QObject>

class SmdPadCalculator : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qreal leadSpanMin READ leadSpanMin WRITE setLeadSpanMin NOTIFY leadSpanMinChanged)
    Q_PROPERTY(qreal leadSpanMax READ leadSpanMax WRITE setLeadSpanMax NOTIFY leadSpanMaxChanged)
    Q_PROPERTY(qreal leadWidthMin READ leadWidthMin WRITE setLeadWidthMin NOTIFY leadWidthMinChanged)
    Q_PROPERTY(qreal leadWidthMax READ leadWidthMax WRITE setLeadWidthMax NOTIFY leadWidthMaxChanged)
    Q_PROPERTY(qreal leadHeightMin READ leadHeightMin WRITE setLeadHeightMin NOTIFY leadHeightMinChanged)
    Q_PROPERTY(qreal leadHeightMax READ leadHeightMax WRITE setLeadHeightMax NOTIFY leadHeightMaxChanged)

    Q_PROPERTY(qreal fabricationTolerance READ fabricationTolerance WRITE setFabricationTolerance NOTIFY fabricationToleranceChanged)
    Q_PROPERTY(qreal placementTolerance READ placementTolerance WRITE setPlacementTolerance NOTIFY placementToleranceChanged)

    Q_PROPERTY(qreal toeGoal READ toeGoal WRITE setToeGoal NOTIFY toeGoalChanged)
    Q_PROPERTY(qreal heelGoal READ heelGoal WRITE setHeelGoal NOTIFY heelGoalChanged)
    Q_PROPERTY(qreal sideGoal READ sideGoal WRITE setSideGoal NOTIFY sideGoalChanged)

    Q_PROPERTY(qreal positionRounding READ positionRounding WRITE setPositionRounding NOTIFY positionRoundingChanged)
    Q_PROPERTY(qreal sizeRounding READ sizeRounding WRITE setSizeRounding NOTIFY sizeRoundingChanged)

    Q_PROPERTY(qreal padSpacing READ padSpacing NOTIFY padSpacingChanged)
    Q_PROPERTY(qreal padWidth READ padWidth NOTIFY padWidthChanged)
    Q_PROPERTY(qreal padHeight READ padHeight NOTIFY padHeightChanged)

public:
    explicit SmdPadCalculator(QObject *parent = 0);
    ~SmdPadCalculator();

    qreal leadSpanMin() const;
    qreal leadSpanMax() const;
    qreal leadWidthMin() const;
    qreal leadWidthMax() const;
    qreal leadHeightMin() const;
    qreal leadHeightMax() const;

    qreal fabricationTolerance() const;
    qreal placementTolerance() const;

    qreal toeGoal() const;
    qreal heelGoal() const;
    qreal sideGoal() const;

    qreal positionRounding() const;
    qreal sizeRounding() const;

    qreal padSpacing() const;
    qreal padWidth() const;
    qreal padHeight() const;

signals:
    void leadSpanMinChanged(qreal leadSpanMin);
    void leadSpanMaxChanged(qreal leadSpanMax);
    void leadWidthMinChanged(qreal leadWidthMin);
    void leadWidthMaxChanged(qreal leadWidthMax);
    void leadHeightMinChanged(qreal leadHeightMin);
    void leadHeightMaxChanged(qreal leadHeightMax);
    void fabricationToleranceChanged(qreal fabricationTolerance);
    void placementToleranceChanged(qreal placementTolerance);
    void toeGoalChanged(qreal toeGoal);
    void heelGoalChanged(qreal heelGoal);
    void sideGoalChanged(qreal sideGoal);
    void positionRoundingChanged(qreal positionRounding);
    void sizeRoundingChanged(qreal sizeRounding);

    void padSpacingChanged(qreal padSpacing);
    void padWidthChanged(qreal padWidth);
    void padHeightChanged(qreal padHeight);

public slots:
    void setLeadSpanMin(qreal leadSpanMin);
    void setLeadSpanMax(qreal leadSpanMax);
    void setLeadWidthMin(qreal leadWidthMin);
    void setLeadWidthMax(qreal leadWidthMax);
    void setLeadHeightMin(qreal leadHeightMin);
    void setLeadHeightMax(qreal leadHeightMax);
    void setFabricationTolerance(qreal fabricationTolerance);
    void setPlacementTolerance(qreal placementTolerance);
    void setToeGoal(qreal toeGoal);
    void setHeelGoal(qreal heelGoal);
    void setSideGoal(qreal sideGoal);
    void setPositionRounding(qreal positionRounding);
    void setSizeRounding(qreal sizeRounding);

    void clear();
    bool calculate();

private:
    void setPadSpacing(qreal padSpacing);
    void setPadWidth(qreal padWidth);
    void setPadHeight(qreal padHeight);

private:
    qreal m_leadSpanMin;
    qreal m_leadSpanMax;
    qreal m_leadWidthMin;
    qreal m_leadWidthMax;
    qreal m_leadHeightMin;
    qreal m_leadHeightMax;
    qreal m_fabricationTolerance;
    qreal m_placementTolerance;
    qreal m_toeGoal;
    qreal m_heelGoal;
    qreal m_sideGoal;
    qreal m_positionRounding;
    qreal m_sizeRounding;
    qreal m_padSpacing;
    qreal m_padWidth;
    qreal m_padHeight;
};

#endif // SMDPADCALCULATOR_H
