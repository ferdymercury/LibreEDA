#-------------------------------------------------
#
# Project created by QtCreator 2017-02-08T12:45:08
#
#-------------------------------------------------

QT       += core gui widgets
CONFIG +=  c++11
TARGET = LePackageDesigner
TEMPLATE = app

QMAKE_CXXFLAGS += \
-Wshadow \
-Wredundant-decls \
-Wcast-align \
-Wmissing-declarations \
-Wmissing-include-dirs \
-Wextra \
-Wall \
-Winvalid-pch \
-Wredundant-decls \
-Wmissing-prototypes \
-Wformat=2 \
-Wmissing-format-attribute \
-Wformat-nonliteral \
-Wuninitialized \
-Wpedantic

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
INCLUDEPATH += $$PWD

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        MainWindow.cpp \
    GraphicsItems.cpp \
    GraphicsLayerStackModel.cpp \
    PropertyManager.cpp \
    AbstractTask.cpp \
    TaskWidget.cpp \
    \
    LandPatternEditor/RenumberTerminalsTask.cpp \
    LandPatternEditor/PlaceArrangementTask.cpp \
    LandPatternEditor/LandPatternWidget.cpp \
    \
    DocumentWidget.cpp \
    DocumentTreeModel.cpp \
    ObjectPropertyBrowser.cpp \
    EditorWidget.cpp \
    PadStackEditor/PadStackWidget.cpp \
    PadStackEditor/PthPadCalculator.cpp \
    PadStackEditor/PthPadCalculatorWidget.cpp \
    PadStackEditor/SmdPadCalculator.cpp \
    PadStackEditor/SmdPadCalculatorWidget.cpp \
    PadStackEditor/PlaceCircularPadTask.cpp \
    PadStackEditor/PlaceRectangularPadTask.cpp \
    GraphicsSceneInspector.cpp \
    PadStackEditor/GraphicsPadStackElementItem.cpp \
    PadStackEditor/PadStackTask.cpp \
    GraphicsCircularShapeItem.cpp \
    GraphicsRectangularShapeItem.cpp \
    LandPatternEditor/QuadInLineArrangementItem.cpp \
    DocumentObjectTreeModel.cpp \
    DocumentObjectInspector.cpp \
    GraphicsTrianglePrimitive.cpp \
    PadStackEditor/PlaceTrianglePrimitive.cpp \
    GraphicsButterflyPrimitive.cpp \
    PadStackEditor/PlaceButterflyPrimitive.cpp \
    GraphicsRegularPolygonPrimitive.cpp \
    PadStackEditor/PlaceRegularPolygonPrimitive.cpp \
    GraphicsArcOfCircle.cpp \
    PadStackEditor/PlaceArcOfCircle.cpp

HEADERS  += MainWindow.h \
    GraphicsItems.h \
    GraphicsLayerStackModel.h \
    PropertyManager.h \
    AbstractTask.h \
    TaskWidget.h \
    DocumentWidget.h \
    DocumentTreeModel.h \
    ObjectPropertyBrowser.h \
    EditorWidget.h \
    \
    LandPatternEditor/RenumberTerminalsTask.h \
    LandPatternEditor/PlaceArrangementTask.h \
    LandPatternEditor/LandPatternWidget.h \
    \
    PadStackEditor/PadStackWidget.h \
    PadStackEditor/PthPadCalculator.h \
    PadStackEditor/PthPadCalculatorWidget.h \
    PadStackEditor/SmdPadCalculator.h \
    PadStackEditor/SmdPadCalculatorWidget.h \
    PadStackEditor/PlaceCircularPadTask.h \
    PadStackEditor/PlaceRectangularPadTask.h \
    GraphicsSceneInspector.h \
    PadStackEditor/GraphicsPadStackElementItem.h \
    PadStackEditor/PadStackTask.h \
    GraphicsCircularShapeItem.h \
    GraphicsRectangularShapeItem.h \
    LandPatternEditor/QuadInLineArrangementItem.h \
    Gui.h \
    DocumentObjectTreeModel.h \
    DocumentObjectInspector.h \
    GraphicsTrianglePrimitive.h \
    PadStackEditor/PlaceTrianglePrimitive.h \
    GraphicsButterflyPrimitive.h \
    PadStackEditor/PlaceButterflyPrimitive.h \
    GraphicsRegularPolygonPrimitive.h \
    PadStackEditor/PlaceRegularPolygonPrimitive.h \
    GraphicsArcOfCircle.h \
    PadStackEditor/PlaceArcOfCircle.h

include(../lib/qtpropertybrowser/qtpropertybrowser.pri)
include(../lib/LeGraphicsView/LeGraphicsView.pri)
include(../lib/LeIpc7351/LeIpc7351.pri)

RESOURCES += \
    LePackageDesigner.qrc

DISTFILES += \
    icons/visibility-on.svg \
    icons/visibility-off.svg

FORMS += \
    PadStackEditor/PthPadCalculatorWidget.ui \
    PadStackEditor/SmdPadCalculatorWidget.ui
