#pragma once

#include <QIcon>

#include "core/core.h"

namespace Gui
{

    static inline QIcon icon(const QString &name)
    {
        return Core::icon(name);
    }

}
