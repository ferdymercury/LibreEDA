#include "MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setObjectName("app");
    app.setApplicationName("LeIpc7351Designer");
    app.setApplicationVersion("0.0");
    app.setApplicationDisplayName("IPC 7351 Designer");
    app.setOrganizationName("LibreEDA");
    app.setOrganizationDomain("www.libre-eda.org");

    MainWindow window;
    window.setObjectName("mainWindow");
    window.show();

    return app.exec();
}
