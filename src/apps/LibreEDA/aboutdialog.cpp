#include "aboutdialog.h"

#include "core/core.h"

#include <QDialogButtonBox>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include <QSysInfo>

AboutDialog::AboutDialog(QWidget *parent):
    QDialog (parent)
{
    setWindowTitle(tr("About Libre EDA"));
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    auto logoLabel = new QLabel;
    QPixmap logo(":/images/leda-dove.png");
    logoLabel->setPixmap(logo);

    const QString text =
            QString("<h3>Libre EDA version %1, build %2</h3>"
                    "Running on %3 (%4)<br/>"
                    "Based on Qt %5 (%6)<br/>"
                    "Compiled with %7<br/>"
                    "Built on %8<br/>"
                    "From revision <a href='%11'>%9%10</a><br/>"
                    "Licensed under the GNU GPL version 3<br/>"
                    "<br/>"
                    "Copyright 2015-2016 Christian Gagneraud<br/>"
                    "Contact: <a href='mailto:chgans&#64;gna.org'>chgans&#64;gna.org</a>")
            .arg(Core::version().toString())
            .arg(Core::buildId())
            .arg(QSysInfo::prettyProductName())
            .arg(QSysInfo::currentCpuArchitecture())
            .arg(Core::qtVersion().toString())
            .arg(QSysInfo::buildAbi())
            .arg(Core::compilerId())
            .arg(Core::buildDateTime().toString(Qt::DefaultLocaleShortDate))
            .arg(Core::sourceId())
            .arg(Core::sourceIdIsTainted() ? " (tainted)" : "")
            .arg(Core::browsableSourceUrl().toString());
    auto textLabel = new QLabel(text);
    textLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);

    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Close);
    QPushButton *closeButton = buttonBox->button(QDialogButtonBox::Close);
    buttonBox->addButton(closeButton, QDialogButtonBox::ButtonRole(QDialogButtonBox::RejectRole | QDialogButtonBox::AcceptRole));
    connect(buttonBox , &QDialogButtonBox::rejected, this, &QDialog::reject);

    auto *layout = new QGridLayout(this);
    layout->setSizeConstraint(QLayout::SetFixedSize);
    layout->addWidget(logoLabel, 0, 0);
    layout->addWidget(textLabel, 0, 1);
    layout->addWidget(buttonBox, 1, 0, 1, 2);
}
