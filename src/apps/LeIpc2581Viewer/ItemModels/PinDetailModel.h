#ifndef PINDETAILMODEL_H
#define PINDETAILMODEL_H

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class Pin;
}

class PinDetailModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    PinDetailModel(QObject *parent = nullptr);

    void setPin(Ipc2581b::Pin *pin);

private:
    Ipc2581b::Pin *m_pin;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};

#endif // PINDETAILMODEL_H