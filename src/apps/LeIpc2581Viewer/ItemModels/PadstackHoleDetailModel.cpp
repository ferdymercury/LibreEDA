#include "PadstackHoleDetailModel.h"
#include "enumtranslator.h"
#include "PadstackHoleDef.h"

enum
{
    NameRow = 0,
    DiameterRow,
    PlatingStatusRow,
    ToleranceRow,
    LocationRow,
    RowCount
};

enum
{
    PropertyNameColumn = 0,
    PropertyValueColumn,
    ColumnCount
};

PadstackHoleDetailModel::PadstackHoleDetailModel(QObject *parent):
    QAbstractTableModel(parent),
    m_holeDef(nullptr)
{

}

void PadstackHoleDetailModel::setPadstackHoleDef(Ipc2581b::PadstackHoleDef *holeDef)
{
    beginResetModel();
    m_holeDef = holeDef;
    endResetModel();
}


int PadstackHoleDetailModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (m_holeDef != nullptr)
        return RowCount;
    return 0;
}

int PadstackHoleDetailModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ColumnCount;
}

QVariant PadstackHoleDetailModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    switch (index.column())
    {
        case PropertyNameColumn:
            switch (index.row())
            {
                case NameRow:
                    return QStringLiteral("Name");
                case DiameterRow:
                    return QStringLiteral("Finished diameter");
                case PlatingStatusRow:
                    return QStringLiteral("Plating");
                case ToleranceRow:
                    return QStringLiteral("Tolerance");
                case LocationRow:
                    return QStringLiteral("Location");
                default:
                    return QVariant();
            }
        case PropertyValueColumn:
            switch (index.row())
            {
                case NameRow:
                    return m_holeDef->name;
                case DiameterRow:
                    return m_holeDef->diameter;
                case PlatingStatusRow:
                    return Ipc2581b::EnumTranslator::platingStatusText(m_holeDef->platingStatus);
                case ToleranceRow:
                    return QString("+%1/-%2").arg(m_holeDef->plusTol).arg(m_holeDef->minusTol);
                case LocationRow:
                    return QString("[%1, %2]").arg(m_holeDef->x, m_holeDef->y);
                default:
                    return QVariant();
            }
        default:
            return QVariant();
    }
}

QVariant PadstackHoleDetailModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    if (section == PropertyNameColumn)
        return QStringLiteral("Property");

    if (section == PropertyValueColumn)
        return QStringLiteral("Value");

    return QVariant();
}
