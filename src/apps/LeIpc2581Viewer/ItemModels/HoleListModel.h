#pragma once

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class Hole;
}

class HoleListModelPrivate;
class HoleListModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(HoleListModel)
    QScopedPointer<HoleListModelPrivate> const d_ptr;

public:
    enum
    {
        NameColumn = 0,
        DiameterColumn,
        PlatingColumn,
        ToleranceColumn,
        LocationColumn,
        ColumnCount
    };

    explicit HoleListModel(QObject *parent = 0);
    ~HoleListModel();

    void addHole(const Ipc2581b::Hole *hole);

public slots:
    void clear();

    // QAbstractTableModel Interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};
