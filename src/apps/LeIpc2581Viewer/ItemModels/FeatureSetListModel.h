#pragma once

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class Set;
}

class FeatureSetListModelPrivate;
class FeatureSetListModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(FeatureSetListModel)
    QScopedPointer<FeatureSetListModelPrivate> const d_ptr;

public:
    enum
    {
        NetColumn = 0,
        PolarityColumn,
        PadUsageColumn,
        TestPointColumn,
        GeometryColumn,
        PlateColumn,
        ComponentColumn,
        ColumnCount
    };

    explicit FeatureSetListModel(QObject *parent = 0);
    ~FeatureSetListModel();

    void addFeatureSet(const Ipc2581b::Set *set);

public slots:
    void clear();

    // QAbstractTableModel Interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};
