#pragma once

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class Layer;
}

class LeGraphicsItemLayer;

class LayerSetModelPrivate;
class LayerSetModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(LayerSetModel)
    QScopedPointer<LayerSetModelPrivate> const d_ptr;

public:
    enum
    {
        EnabledColumn = 0,
        VisibleColumn,
        SequenceNumberColumn,
        NameColumn,
        PadVisibleColumn,
        PadBrushColumn,
        PinVisibleColumn,
        PinBrushColumn,
        FiducialVisibleColumn,
        FiducialBrushColumn,
        HoleVisibleColumn,
        HoleBrushColumn,
        SlotVisibleColumn,
        SlotBrushColumn,
        FillVisibleColumn,
        FillBrushColumn,
        TraceVisibleColumn,
        TraceBrushColumn,
        TextVisibleColumn,
        TextBrushColumn,
        ColumnCount
    };

    explicit LayerSetModel(QObject *parent = 0);
    ~LayerSetModel();

    void addLayer(int sequenceNumber, bool enabled, const Ipc2581b::Layer *ipcLayer, LeGraphicsItemLayer *graphicsLayer);

    QList<LeGraphicsItemLayer*> disabledGraphicsLayers() const;
    QList<LeGraphicsItemLayer*> enabledGraphicsLayers() const;
    QList<LeGraphicsItemLayer*> graphicsLayers() const;
    int layerSequenceNumber(const QString &ipcName) const;
    LeGraphicsItemLayer *graphicsLayer(const QString &ipcName) const;
    LeGraphicsItemLayer *graphicsLayer(const QModelIndex &index) const;

    QModelIndex moveToTop(const QModelIndex &index);
    QModelIndex moveToBottom(const QModelIndex &index);
    QModelIndex moveUp(const QModelIndex &index);
    QModelIndex moveDown(const QModelIndex &index);

signals:
    void layerEnabledChanged();
    void layerOrderChanged();

public slots:
    void clear();

    // QAbstractTableModel Interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // QAbstractItemModel interface
public:
    virtual bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild) override;
};
