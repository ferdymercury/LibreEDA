#pragma once

#include <QMap>
#include <QObject>
#include <QScopedPointer>

namespace Ipc2581b
{
    class Ipc2581;
    class Content;
    class Step;
    class Contour;
}

class ComponentListModel;
class HoleListModel;
class FeatureSetListModel;
class LayerListModel;
class NetListModel;
class PackageListModel;
class PadListModel;
class SpecificationTreeModel;
class LayerStackupModel;

class Document : public QObject
{
    Q_OBJECT
public:
    explicit Document(QObject *parent = 0);
    ~Document();

    bool open(const QString &fileName);
    QString errorString() const;

    const Ipc2581b::Content *content() const;
    QString stepName() const;
    const Ipc2581b::Contour *stepProfile() const;
    QPointF stepDatum() const;

    LayerListModel *layerListModel() const;
    ComponentListModel *componentListModel() const;
    PackageListModel *packageListModel() const;
    NetListModel *netListModel() const;
    PadListModel *padListModel() const;
    HoleListModel *holeListModel() const;
    FeatureSetListModel *featureSetListModel() const;
    SpecificationTreeModel *specificationTreeModel() const;
    LayerStackupModel *layerStackModel() const;

signals:

public slots:

private:
    QString m_errorString;

    QScopedPointer<Ipc2581b::Ipc2581> m_ipc;
    const Ipc2581b::Step *m_step = nullptr;

    SpecificationTreeModel *m_specTreeModel = nullptr;
    LayerListModel *m_layerListModel = nullptr;
    ComponentListModel *m_componentListModel = nullptr;
    PackageListModel *m_packageListModel = nullptr;
    NetListModel *m_netListModel = nullptr;
    PadListModel *m_padListModel = nullptr;
    HoleListModel *m_holeListModel = nullptr;
    QMap<QString, FeatureSetListModel*> m_featureSetListModelMap;
    QMap<QString, LayerStackupModel*> m_layerStackModelMap;
};

Q_DECLARE_METATYPE(Document*)
Q_DECLARE_METATYPE(const Document*)
