#pragma once

#include <QComboBox>

class PenStyleComboBox : public QComboBox
{
    Q_OBJECT
    Q_PROPERTY(Qt::PenStyle currentPenStyle READ currentPenStyle NOTIFY currentPenStyleChanged)
    Q_PROPERTY(QList<Qt::PenStyle> penStyles READ penStyles WRITE setPenStyles)

public:
    explicit PenStyleComboBox(QWidget *parent = nullptr);
    ~PenStyleComboBox();

    void setCurrentPenStyle(Qt::PenStyle style);
    Qt::PenStyle currentPenStyle() const;

    void setPenStyles(const QList<Qt::PenStyle> &styles);
    QList<Qt::PenStyle> penStyles() const;

signals:
    void activated(Qt::PenStyle);
    void highlighted(Qt::PenStyle);
    void currentPenStyleChanged(Qt::PenStyle);

private slots:
    void onActivated(int index);
    void onHighlighted(int index);
    void onCurrentIndexChanged(int index);

private:
    Q_DISABLE_COPY(PenStyleComboBox)
    QList<Qt::PenStyle> m_penStyleList;
    Qt::PenStyle m_paintedPenStyle;

    // QComboBox Interface
protected:
   virtual void paintEvent(QPaintEvent *event) override;
};
