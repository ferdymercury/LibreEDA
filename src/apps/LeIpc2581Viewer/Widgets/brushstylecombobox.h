#pragma once

#include <QComboBox>

class BrushStyleComboBox : public QComboBox
{
    Q_OBJECT
    Q_PROPERTY(Qt::BrushStyle currentBrushStyle READ currentBrushStyle NOTIFY currentBrushStyleChanged)
    Q_PROPERTY(QList<Qt::BrushStyle> BrushStyles READ brushStyles WRITE setBrushStyles)

public:
    explicit BrushStyleComboBox(QWidget *parent = nullptr);
    ~BrushStyleComboBox();

    void setCurrentBrushStyle(Qt::BrushStyle style);
    Qt::BrushStyle currentBrushStyle() const;

    void setBrushStyles(const QList<Qt::BrushStyle> &styles);
    QList<Qt::BrushStyle> brushStyles() const;

signals:
    void activated(Qt::BrushStyle style);
    void highlighted(Qt::BrushStyle style);
    void currentBrushStyleChanged(Qt::BrushStyle style);

private slots:
    void onActivated(int index);
    void onHighlighted(int index);
    void onCurrentIndexChanged(int index);

private:
    Q_DISABLE_COPY(BrushStyleComboBox)
    QList<Qt::BrushStyle> m_brushStyleList;
    Qt::BrushStyle m_paintedBrushStyle;

    // QComboBox Interface
protected:
   virtual void paintEvent(QPaintEvent *event) override;
};
