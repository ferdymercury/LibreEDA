#pragma once

#include "LeGraphicsView.h"

#include <QObject>
#include <QRectF>

namespace LeGraphicsView
{
    class Ruler;

    class OLDGRAPHICSVIEW_EXPORT GridModel : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(qreal minimumSceneStep READ minimumSceneStep WRITE setMinimumSceneStep NOTIFY minimumSceneStepChanged)
        Q_PROPERTY(int minimumTickSpacing READ minimumTickSpacing WRITE setMinimumTickSpacing NOTIFY minimumTickSpacingChanged)
        Q_PROPERTY(int coarseMultiplier READ coarseMultiplier WRITE setCoarseMultiplier NOTIFY coarseMultiplierChanged)
        Q_PROPERTY(QRectF sceneRect READ sceneRect WRITE setSceneRect NOTIFY sceneRectChanged)
        Q_PROPERTY(QSize size READ size WRITE setSize NOTIFY sizeChanged)

    public:
        explicit GridModel(QObject *parent = 0);

        int minimumTickSpacing() const;
        int coarseMultiplier() const;
        QSize size() const;
        qreal minimumSceneStep() const;
        QRectF sceneRect() const;

        QVector<qreal> majorXValues() const;
        QVector<qreal> minorXValues() const;
        QVector<int> majorXTicks() const;
        QVector<int> minorXTicks() const;
        int xTickPosition(qreal pos);
        QVector<qreal> majorYValues() const;
        QVector<qreal> minorYValues() const;
        QVector<int> majorYTicks() const;
        QVector<int> minorYTicks() const;
        int yTickPosition(qreal pos);

        QPointF round(const QPointF &scenePos) const;

    signals:
        void minimumSceneStepChanged(qreal step);
        void minimumTickSpacingChanged(int spacing);
        void coarseMultiplierChanged(int multiplier);
        void sceneRectChanged(const QRectF &rect);
        void sizeChanged(QSize size);

    public slots:
        void setMinimumSceneStep(qreal step);
        void setMinimumTickSpacing(int spacing);
        void setCoarseMultiplier(int multiplier);
        void setSceneRect(const QRectF &rect);
        void setSize(QSize size);

    private:
        Ruler *m_horizontalRuler;
        Ruler *m_verticalRuler;
        qreal m_minimumSceneStep;
        int m_minimumTickSpacing;
        int m_coarseMultiplier;
        QRectF m_sceneRect;
        QSize m_size;
    };

}
