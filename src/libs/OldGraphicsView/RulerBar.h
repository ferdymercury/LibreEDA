#pragma once

#include "LeGraphicsView.h"

#include <QWidget>

namespace LeGraphicsView
{

    class GridModel;

    class OLDGRAPHICSVIEW_EXPORT RulerBar : public QWidget
    {
        Q_OBJECT
        Q_ENUMS(Alignment)

    public:
        enum Alignment { Horizontal, Vertical };
        static const int BREADTH;

        explicit RulerBar(RulerBar::Alignment alignment, QWidget *parent = nullptr);
        ~RulerBar();

        void setAlignment(RulerBar::Alignment alignment);
        RulerBar::Alignment rulerType() const;

        void setGridModel(GridModel *model);
        GridModel *gridModel() const;

        void setCursorPosition(const QPointF &pos);
        QPointF cursorPosition() const;

        void setBackgroundColor(const QColor &color);
        QColor backgroundColor() const;

        void setForegroundColor(const QColor &color);
        QColor foregroundColor() const;

    private:
        static const int FONT_SIZE;
        static const int MAJOR_TICK_HEIGHT;
        static const int MINOR_TICK_HEIGHT;

        // Store a Graduator GridModel::[x|y]Graduator() ?
        GridModel *m_gridModel;
        Alignment m_alignment;
        QColor m_backgroundColor;
        QColor m_foregroundColor;
        qreal m_currentPos;

        void drawMinorTick(QPainter &painter, int pixelPos);
        void drawMajorTick(QPainter &painter, int pixelPos, qreal logicalPos);
        void drawIndicator(QPainter &painter, int pixelPos);

        // QWidget interface
    public:
        // TODO: get rid of this
        QSize minimumSizeHint() const override;

        // QWidget interface
    protected:
        void paintEvent(QPaintEvent *event) override;
    };

}
