#pragma once

#include <QtCore/qglobal.h>

#if defined(LEGERBER_LIBRARY)
#  define LEGERBER_EXPORT Q_DECL_EXPORT
#else
#  define LEGERBER_EXPORT Q_DECL_IMPORT
#endif
