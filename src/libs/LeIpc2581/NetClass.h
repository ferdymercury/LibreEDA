
#pragma once

namespace Ipc2581b
{

enum class NetClass
{
    Ground,
    Power,
    Unused,
    Fixed,
    Clk,
    Signal,
};

}