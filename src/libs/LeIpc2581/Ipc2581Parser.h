
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Ipc2581.h"

namespace Ipc2581b
{

class RevisionParser;
class ContentParser;
class ECadParser;
class LogisticHeaderParser;
class HistoryRecordParser;
class BomParser;
class AvlParser;

class Ipc2581Parser
{
public:
    Ipc2581Parser(
            RevisionParser*&
            , ContentParser*&
            , LogisticHeaderParser*&
            , HistoryRecordParser*&
            , BomParser*&
            , ECadParser*&
            , AvlParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Ipc2581 *result();

private:
    RevisionParser *&m_revisionParser;
    ContentParser *&m_contentParser;
    LogisticHeaderParser *&m_logisticHeaderParser;
    HistoryRecordParser *&m_historyRecordParser;
    BomParser *&m_bomParser;
    ECadParser *&m_ecadParser;
    AvlParser *&m_avlParser;
    QScopedPointer<Ipc2581> m_result;
};

}