#include "BomDesParser.h"

#include "MatDesParser.h"
#include "RefDesParser.h"
#include "DocDesParser.h"
#include "ToolDesParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

BomDesParser::BomDesParser(
    DocDesParser *&_docDesParser
    , MatDesParser *&_matDesParser
    , RefDesParser *&_refDesParser
    , ToolDesParser *&_toolDesParser
):    m_docDesParser(_docDesParser)
    , m_matDesParser(_matDesParser)
    , m_refDesParser(_refDesParser)
    , m_toolDesParser(_toolDesParser)
{

}

bool BomDesParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("DocDes"))
    {
        if(m_docDesParser->parse(reader))
        {
            m_result = m_docDesParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("MatDes"))
    {
        if(m_matDesParser->parse(reader))
        {
            m_result = m_matDesParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("RefDes"))
    {
        if(m_refDesParser->parse(reader))
        {
            m_result = m_refDesParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("ToolDes"))
    {
        if(m_toolDesParser->parse(reader))
        {
            m_result = m_toolDesParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"BomDes\"").arg(reader->name().toString()));
    return false;
}

BomDes *BomDesParser::result()
{
    return m_result;
}

bool BomDesParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("DocDes"))
        return true;
    if (name == QStringLiteral("MatDes"))
        return true;
    if (name == QStringLiteral("RefDes"))
        return true;
    if (name == QStringLiteral("ToolDes"))
        return true;
    return false;
}

}