
#pragma once

namespace Ipc2581b
{

enum class ModRef
{
    Design,
    Assembly,
    Fabrication,
    Test,
    Userdef,
};

}