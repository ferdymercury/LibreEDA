
#pragma once

#include <QList>
#include "Optional.h"


#include "Pad.h"
#include "Target.h"

namespace Ipc2581b
{

class LandPattern
{
public:
	virtual ~LandPattern() {}

    QList<Pad*> padList;
    QList<Target*> targetList;

};

}