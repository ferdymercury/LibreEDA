
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "NameRef.h"

namespace Ipc2581b
{

class QStringParser;

class NameRefParser
{
public:
    NameRefParser(
            QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    NameRef *result();

private:
    QStringParser *&m_nameParser;
    QScopedPointer<NameRef> m_result;
};

}