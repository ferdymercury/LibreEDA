
#pragma once

namespace Ipc2581b
{

enum class ToolList
{
    Router,
    VCutter,
    Extension,
    Laser,
    Carbide,
    Flatnose,
};

}