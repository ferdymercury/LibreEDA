
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "LineDescRef.h"

namespace Ipc2581b
{

class QStringParser;

class LineDescRefParser
{
public:
    LineDescRefParser(
            QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    LineDescRef *result();

private:
    QStringParser *&m_idParser;
    QScopedPointer<LineDescRef> m_result;
};

}