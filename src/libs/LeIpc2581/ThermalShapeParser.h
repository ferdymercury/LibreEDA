
#pragma once

#include <QString>

class QXmlStreamReader;

#include "ThermalShape.h"

namespace Ipc2581b
{

class ThermalShapeParser
{
public:
    ThermalShapeParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    ThermalShape result();

private:
    ThermalShape m_result;
};

}