#include "PinOneOrientationParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

PinOneOrientationParser::PinOneOrientationParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool PinOneOrientationParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, PinOneOrientation> map =
    {
        {QStringLiteral("LOWER_LEFT"), PinOneOrientation::LowerLeft},
        {QStringLiteral("UPPER_CENTER"), PinOneOrientation::UpperCenter},
        {QStringLiteral("LEFT_CENTER"), PinOneOrientation::LeftCenter},
        {QStringLiteral("OTHER"), PinOneOrientation::Other},
        {QStringLiteral("UPPER_LEFT"), PinOneOrientation::UpperLeft},
        {QStringLiteral("LEFT"), PinOneOrientation::Left},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum PinOneOrientation").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

PinOneOrientation PinOneOrientationParser::result()
{
    return m_result;
}

}