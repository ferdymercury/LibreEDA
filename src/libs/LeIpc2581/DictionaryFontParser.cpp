#include "DictionaryFontParser.h"

#include "UnitsParser.h"
#include "EntryFontParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

DictionaryFontParser::DictionaryFontParser(
    UnitsParser *&_unitsParser
    , EntryFontParser *&_entryFontParser
):    m_unitsParser(_unitsParser)
    , m_entryFontParser(_entryFontParser)
{

}

bool DictionaryFontParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new DictionaryFont());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("units")))
    {
        data = reader->attributes().value(QStringLiteral("units"));
        if (!m_unitsParser->parse(reader, data))
            return false;
        m_result->units = m_unitsParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("units: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("EntryFont"))
        {
            if (!m_entryFontParser->parse(reader))
                return false;
            auto result = m_entryFontParser->result();
            m_result->entryFontList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

DictionaryFont *DictionaryFontParser::result()
{
    return m_result.take();
}

}