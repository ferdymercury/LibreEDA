
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "TechnologyList.h"
#include "Property.h"
#include "QString.h"

namespace Ipc2581b
{

class Technology: public SpecificationType
{
public:
	virtual ~Technology() {}

    TechnologyList type;
    Optional<QString> commentOptional;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::Technology;
    }
};

}