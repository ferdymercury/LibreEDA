
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "Xform.h"
#include "LineDescGroup.h"
#include "Double.h"
#include "ThermalShape.h"
#include "FillDescGroup.h"
#include "Int.h"

namespace Ipc2581b
{

class Thermal: public StandardPrimitive
{
public:
	virtual ~Thermal() {}

    ThermalShape shape;
    Double outerDiameter;
    Double innerDiameter;
    Optional<Int> spokeCountOptional;
    Optional<Double> gapOptional;
    Double spokeStartAngle;
    Optional<Xform*> xformOptional;
    Optional<LineDescGroup*> lineDescGroupOptional;
    Optional<FillDescGroup*> fillDescGroupOptional;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::Thermal;
    }
};

}