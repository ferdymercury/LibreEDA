#include "LineDescRefParser.h"

#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

LineDescRefParser::LineDescRefParser(
    QStringParser *&_idParser
):    m_idParser(_idParser)
{

}

bool LineDescRefParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new LineDescRef());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("id")))
    {
        data = reader->attributes().value(QStringLiteral("id"));
        if (!m_idParser->parse(reader, data))
            return false;
        m_result->id = m_idParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("id: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

LineDescRef *LineDescRefParser::result()
{
    return m_result.take();
}

}