
#pragma once

namespace Ipc2581b
{

enum class CertificationCategory
{
    Fabricationdrawing,
    Generalassembly,
    Gluedot,
    Embeddedcomponent,
    Detaileddrawing,
    Specsourcecontroldrawing,
    Assemblypanel,
    Componentplacement,
    Other,
    Assemblytestgeneration,
    Mechanicalhardware,
    Multiboardpartslist,
    Boardfabrication,
    Boardpanel,
    Singleboardpartslist,
    Assemblytestfixturegeneration,
    Assemblydrawing,
    Solderstencilpaste,
    Phototools,
    Boardtestgeneration,
    Assemblyfixturegeneration,
    Schematicdrawings,
    Assemblypreptools,
    Boardfixturegeneration,
};

}