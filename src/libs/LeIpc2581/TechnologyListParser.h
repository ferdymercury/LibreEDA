
#pragma once

#include <QString>

class QXmlStreamReader;

#include "TechnologyList.h"

namespace Ipc2581b
{

class TechnologyListParser
{
public:
    TechnologyListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    TechnologyList result();

private:
    TechnologyList m_result;
};

}