#include "NonstandardAttributeParser.h"

#include "CadPropertyParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

NonstandardAttributeParser::NonstandardAttributeParser(
    QStringParser *&_nameParser
    , CadPropertyParser *&_typeParser
    , QStringParser *&_valueParser
):    m_nameParser(_nameParser)
    , m_typeParser(_typeParser)
    , m_valueParser(_valueParser)
{

}

bool NonstandardAttributeParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new NonstandardAttribute());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("type")))
    {
        data = reader->attributes().value(QStringLiteral("type"));
        if (!m_typeParser->parse(reader, data))
            return false;
        m_result->type = m_typeParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("type: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("value")))
    {
        data = reader->attributes().value(QStringLiteral("value"));
        if (!m_valueParser->parse(reader, data))
            return false;
        m_result->value = m_valueParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("value: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

NonstandardAttribute *NonstandardAttributeParser::result()
{
    return m_result.take();
}

}