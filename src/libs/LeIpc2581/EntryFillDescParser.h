
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "EntryFillDesc.h"

namespace Ipc2581b
{

class FillDescParser;
class QStringParser;

class EntryFillDescParser
{
public:
    EntryFillDescParser(
            QStringParser*&
            , FillDescParser*&
            );

    bool parse(QXmlStreamReader *reader);
    EntryFillDesc *result();

private:
    QStringParser *&m_idParser;
    FillDescParser *&m_fillDescParser;
    QScopedPointer<EntryFillDesc> m_result;
};

}