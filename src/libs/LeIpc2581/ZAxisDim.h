
#pragma once


namespace Ipc2581b
{

class MaterialCut;
class MaterialLeft;

class ZAxisDim
{
public:
	virtual ~ZAxisDim() {}

    enum class ZAxisDimType
    {
        MaterialCut
        ,MaterialLeft
    };

    virtual ZAxisDimType zAxisDimType() const = 0;

    inline bool isMaterialCut() const
    {
        return zAxisDimType() == ZAxisDimType::MaterialCut;
    }

    inline MaterialCut *toMaterialCut()
    {
        return reinterpret_cast<MaterialCut*>(this);
    }

    inline const MaterialCut *toMaterialCut() const
    {
        return reinterpret_cast<const MaterialCut*>(this);
    }

    inline bool isMaterialLeft() const
    {
        return zAxisDimType() == ZAxisDimType::MaterialLeft;
    }

    inline MaterialLeft *toMaterialLeft()
    {
        return reinterpret_cast<MaterialLeft*>(this);
    }

    inline const MaterialLeft *toMaterialLeft() const
    {
        return reinterpret_cast<const MaterialLeft*>(this);
    }


};

}

// For user convenience
#include "MaterialCut.h"
#include "MaterialLeft.h"
