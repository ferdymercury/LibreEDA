
#pragma once

#include <QString>

class QXmlStreamReader;

namespace Ipc2581b
{

class BoolParser
{
public:
    BoolParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    bool result();

private:
    bool m_result;
};

}