#include "DielectricListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

DielectricListParser::DielectricListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool DielectricListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, DielectricList> map =
    {
        {QStringLiteral("DIELECTRIC_CONSTANT"), DielectricList::DielectricConstant},
        {QStringLiteral("RESIN_CONTENT"), DielectricList::ResinContent},
        {QStringLiteral("PROCESSABILITY_TEMP"), DielectricList::ProcessabilityTemp},
        {QStringLiteral("OTHER"), DielectricList::Other},
        {QStringLiteral("LOSS_TANGENT"), DielectricList::LossTangent},
        {QStringLiteral("GLASS_STYLE"), DielectricList::GlassStyle},
        {QStringLiteral("GLASS_TYPE"), DielectricList::GlassType},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum DielectricList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

DielectricList DielectricListParser::result()
{
    return m_result;
}

}