#include "DictionaryLineDescParser.h"

#include "UnitsParser.h"
#include "EntryLineDescParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

DictionaryLineDescParser::DictionaryLineDescParser(
    UnitsParser *&_unitsParser
    , EntryLineDescParser *&_entryLineDescParser
):    m_unitsParser(_unitsParser)
    , m_entryLineDescParser(_entryLineDescParser)
{

}

bool DictionaryLineDescParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new DictionaryLineDesc());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("units")))
    {
        data = reader->attributes().value(QStringLiteral("units"));
        if (!m_unitsParser->parse(reader, data))
            return false;
        m_result->units = m_unitsParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("units: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("EntryLineDesc"))
        {
            if (!m_entryLineDescParser->parse(reader))
                return false;
            auto result = m_entryLineDescParser->result();
            m_result->entryLineDescList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

DictionaryLineDesc *DictionaryLineDescParser::result()
{
    return m_result.take();
}

}