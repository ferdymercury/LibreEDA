
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "SoftwarePackage.h"

namespace Ipc2581b
{

class CertificationParser;
class QStringParser;

class SoftwarePackageParser
{
public:
    SoftwarePackageParser(
            QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , CertificationParser*&
            );

    bool parse(QXmlStreamReader *reader);
    SoftwarePackage *result();

private:
    QStringParser *&m_nameParser;
    QStringParser *&m_vendorParser;
    QStringParser *&m_revisionParser;
    QStringParser *&m_modelParser;
    CertificationParser *&m_certificationParser;
    QScopedPointer<SoftwarePackage> m_result;
};

}