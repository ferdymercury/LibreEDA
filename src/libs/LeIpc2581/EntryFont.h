
#pragma once

#include <QList>
#include "Optional.h"


#include "FontDef.h"
#include "QString.h"

namespace Ipc2581b
{

class EntryFont
{
public:
	virtual ~EntryFont() {}

    QString id;
    FontDef *fontDef;

};

}