
#pragma once

#include <QString>

class QXmlStreamReader;

#include "Mount.h"

namespace Ipc2581b
{

class MountParser
{
public:
    MountParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    Mount result();

private:
    Mount m_result;
};

}