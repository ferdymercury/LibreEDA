#include "IntParser.h"
#include <QXmlStreamReader>

namespace Ipc2581b
{

IntParser::IntParser()
{

}

bool IntParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    Q_UNUSED(reader);
    bool ok;
    m_result = data.toInt(&ok, 10);
    if (!ok)
        reader->raiseError(QString("\"%1\": Invalid value for an integer").arg(data.toString()));
    return ok;
}

int IntParser::result()
{
    return m_result;
}

}