
#pragma once

#include <QList>
#include "Optional.h"


#include "Units.h"
#include "EntryFont.h"

namespace Ipc2581b
{

class DictionaryFont
{
public:
	virtual ~DictionaryFont() {}

    Units units;
    QList<EntryFont*> entryFontList;

};

}