
#pragma once

#include <QString>

class QXmlStreamReader;

#include "EnterpriseCode.h"

namespace Ipc2581b
{

class EnterpriseCodeParser
{
public:
    EnterpriseCodeParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    EnterpriseCode result();

private:
    EnterpriseCode m_result;
};

}