
#pragma once

namespace Ipc2581b
{

enum class ThermalShape
{
    Round,
    Hexagon,
    Octagon,
    Square,
};

}