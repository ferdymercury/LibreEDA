#include "RouteParser.h"

#include "LayerFeatureParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

RouteParser::RouteParser(
    QStringParser *&_netParser
    , LayerFeatureParser *&_layerFeatureParser
):    m_netParser(_netParser)
    , m_layerFeatureParser(_layerFeatureParser)
{

}

bool RouteParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Route());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("net")))
    {
        data = reader->attributes().value(QStringLiteral("net"));
        if (!m_netParser->parse(reader, data))
            return false;
        m_result->netOptional = Optional<QString>(m_netParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("LayerFeature"))
        {
            if (!m_layerFeatureParser->parse(reader))
                return false;
            auto result = m_layerFeatureParser->result();
            m_result->layerFeatureList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Route *RouteParser::result()
{
    return m_result.take();
}

}