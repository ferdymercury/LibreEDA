
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Pad.h"

namespace Ipc2581b
{

class LocationParser;
class PinRefParser;
class FeatureParser;
class XformParser;
class QStringParser;

class PadParser
{
public:
    PadParser(
            QStringParser*&
            , XformParser*&
            , LocationParser*&
            , FeatureParser*&
            , PinRefParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Pad *result();

private:
    QStringParser *&m_padstackDefRefParser;
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    FeatureParser *&m_featureParser;
    PinRefParser *&m_pinRefParser;
    QScopedPointer<Pad> m_result;
};

}