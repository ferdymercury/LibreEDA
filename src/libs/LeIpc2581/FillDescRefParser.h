
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "FillDescRef.h"

namespace Ipc2581b
{

class QStringParser;

class FillDescRefParser
{
public:
    FillDescRefParser(
            QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    FillDescRef *result();

private:
    QStringParser *&m_idParser;
    QScopedPointer<FillDescRef> m_result;
};

}