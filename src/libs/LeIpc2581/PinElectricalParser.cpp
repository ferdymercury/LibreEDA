#include "PinElectricalParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

PinElectricalParser::PinElectricalParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool PinElectricalParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, PinElectrical> map =
    {
        {QStringLiteral("MECHANICAL"), PinElectrical::Mechanical},
        {QStringLiteral("ELECTRICAL"), PinElectrical::Electrical},
        {QStringLiteral("UNDEFINED"), PinElectrical::Undefined},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum PinElectrical").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

PinElectrical PinElectricalParser::result()
{
    return m_result;
}

}