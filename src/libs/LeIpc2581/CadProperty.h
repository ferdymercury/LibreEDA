
#pragma once

namespace Ipc2581b
{

enum class CadProperty
{
    Double,
    Integer,
    Boolean,
    String,
};

}