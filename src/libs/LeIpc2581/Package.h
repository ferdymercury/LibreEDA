
#pragma once

#include <QList>
#include "Optional.h"


#include "PinOneOrientation.h"
#include "PackageType.h"
#include "QString.h"
#include "AssemblyDrawing.h"
#include "Pin.h"
#include "Outline.h"
#include "SilkScreen.h"
#include "Double.h"
#include "LandPattern.h"
#include "Location.h"

namespace Ipc2581b
{

class Package
{
public:
	virtual ~Package() {}

    QString name;
    PackageType type;
    Optional<QString> pinOneOptional;
    Optional<PinOneOrientation> pinOneOrientationOptional;
    Optional<Double> heightOptional;
    Optional<QString> commentOptional;
    Outline *outline;
    Optional<Location*> pickupPointOptional;
    Optional<LandPattern*> landPatternOptional;
    Optional<SilkScreen*> silkScreenOptional;
    Optional<AssemblyDrawing*> assemblyDrawingOptional;
    QList<Pin*> pinList;

};

}