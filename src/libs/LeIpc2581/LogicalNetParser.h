
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "LogicalNet.h"

namespace Ipc2581b
{

class NetClassParser;
class NonstandardAttributeParser;
class PinRefParser;
class QStringParser;

class LogicalNetParser
{
public:
    LogicalNetParser(
            QStringParser*&
            , NetClassParser*&
            , NonstandardAttributeParser*&
            , PinRefParser*&
            );

    bool parse(QXmlStreamReader *reader);
    LogicalNet *result();

private:
    QStringParser *&m_nameParser;
    NetClassParser *&m_netClassParser;
    NonstandardAttributeParser *&m_nonstandardAttributeParser;
    PinRefParser *&m_pinRefParser;
    QScopedPointer<LogicalNet> m_result;
};

}