#include "LineEndParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

LineEndParser::LineEndParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool LineEndParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, LineEnd> map =
    {
        {QStringLiteral("ROUND"), LineEnd::Round},
        {QStringLiteral("SQUARE"), LineEnd::Square},
        {QStringLiteral("NONE"), LineEnd::None},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum LineEnd").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

LineEnd LineEndParser::result()
{
    return m_result;
}

}