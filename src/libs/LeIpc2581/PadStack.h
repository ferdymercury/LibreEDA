
#pragma once

#include <QList>
#include "Optional.h"


#include "LayerPad.h"
#include "LayerHole.h"
#include "QString.h"

namespace Ipc2581b
{

class PadStack
{
public:
	virtual ~PadStack() {}

    Optional<QString> netOptional;
    LayerHole *layerHole;
    QList<LayerPad*> layerPadList;

};

}