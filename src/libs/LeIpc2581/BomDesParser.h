
#pragma once

#include <QString>
class QXmlStreamReader;

#include "BomDes.h"

namespace Ipc2581b
{

class MatDesParser;
class RefDesParser;
class DocDesParser;
class ToolDesParser;

class BomDesParser
{
public:
    BomDesParser(
            DocDesParser*&
            , MatDesParser*&
            , RefDesParser*&
            , ToolDesParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    BomDes *result();

private:
    DocDesParser *&m_docDesParser;
    MatDesParser *&m_matDesParser;
    RefDesParser *&m_refDesParser;
    ToolDesParser *&m_toolDesParser;
    BomDes *m_result;
};

}