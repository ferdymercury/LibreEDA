
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "CadData.h"

namespace Ipc2581b
{

class LayerParser;
class StackupParser;
class StepParser;

class CadDataParser
{
public:
    CadDataParser(
            LayerParser*&
            , StackupParser*&
            , StepParser*&
            );

    bool parse(QXmlStreamReader *reader);
    CadData *result();

private:
    LayerParser *&m_layerParser;
    StackupParser *&m_stackupParser;
    StepParser *&m_stepParser;
    QScopedPointer<CadData> m_result;
};

}