
#pragma once

#include <QList>
#include "Optional.h"


#include "Bool.h"
#include "PhyNet.h"
#include "QString.h"

namespace Ipc2581b
{

class PhyNetGroup
{
public:
	virtual ~PhyNetGroup() {}

    QString name;
    Optional<Bool> optimizedOptional;
    QList<PhyNet*> phyNetList;

};

}