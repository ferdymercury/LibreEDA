
#pragma once

#include <QList>
#include "Optional.h"


#include "Revision.h"
#include "Content.h"
#include "ECad.h"
#include "LogisticHeader.h"
#include "HistoryRecord.h"
#include "Bom.h"
#include "Avl.h"

namespace Ipc2581b
{

class Ipc2581
{
public:
	virtual ~Ipc2581() {}

    Revision revision;
    Content *content;
    LogisticHeader *logisticHeader;
    HistoryRecord *historyRecord;
    QList<Bom*> bomList;
    ECad *ecad;
    Optional<Avl*> avlOptional;

};

}