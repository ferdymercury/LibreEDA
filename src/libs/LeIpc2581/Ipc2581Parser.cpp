#include "Ipc2581Parser.h"

#include "RevisionParser.h"
#include "ContentParser.h"
#include "ECadParser.h"
#include "LogisticHeaderParser.h"
#include "HistoryRecordParser.h"
#include "BomParser.h"
#include "AvlParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

Ipc2581Parser::Ipc2581Parser(
    RevisionParser *&_revisionParser
    , ContentParser *&_contentParser
    , LogisticHeaderParser *&_logisticHeaderParser
    , HistoryRecordParser *&_historyRecordParser
    , BomParser *&_bomParser
    , ECadParser *&_ecadParser
    , AvlParser *&_avlParser
):    m_revisionParser(_revisionParser)
    , m_contentParser(_contentParser)
    , m_logisticHeaderParser(_logisticHeaderParser)
    , m_historyRecordParser(_historyRecordParser)
    , m_bomParser(_bomParser)
    , m_ecadParser(_ecadParser)
    , m_avlParser(_avlParser)
{

}

bool Ipc2581Parser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Ipc2581());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("revision")))
    {
        data = reader->attributes().value(QStringLiteral("revision"));
        if (!m_revisionParser->parse(reader, data))
            return false;
        m_result->revision = m_revisionParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("revision: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Content"))
        {
            if (!m_contentParser->parse(reader))
                return false;
            auto result = m_contentParser->result();
            m_result->content = result;
        }
        else if (name == QStringLiteral("LogisticHeader"))
        {
            if (!m_logisticHeaderParser->parse(reader))
                return false;
            auto result = m_logisticHeaderParser->result();
            m_result->logisticHeader = result;
        }
        else if (name == QStringLiteral("HistoryRecord"))
        {
            if (!m_historyRecordParser->parse(reader))
                return false;
            auto result = m_historyRecordParser->result();
            m_result->historyRecord = result;
        }
        else if (name == QStringLiteral("Bom"))
        {
            if (!m_bomParser->parse(reader))
                return false;
            auto result = m_bomParser->result();
            m_result->bomList.append(result);
        }
        else if (name == QStringLiteral("Ecad"))
        {
            if (!m_ecadParser->parse(reader))
                return false;
            auto result = m_ecadParser->result();
            m_result->ecad = result;
        }
        else if (name == QStringLiteral("Avl"))
        {
            if (!m_avlParser->parse(reader))
                return false;
            auto result = m_avlParser->result();
            m_result->avlOptional = Optional<Avl*>(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Ipc2581 *Ipc2581Parser::result()
{
    return m_result.take();
}

}