#include "ContentParser.h"

#include "DictionaryFontParser.h"
#include "DictionaryLineDescParser.h"
#include "DictionaryFirmwareParser.h"
#include "DictionaryColorParser.h"
#include "QStringParser.h"
#include "NameRefParser.h"
#include "DictionaryFillDescParser.h"
#include "FunctionModeParser.h"
#include "DictionaryStandardParser.h"
#include "DictionaryUserParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

ContentParser::ContentParser(
    QStringParser *&_roleRefParser
    , FunctionModeParser *&_functionModeParser
    , NameRefParser *&_stepRefParser
    , NameRefParser *&_layerRefParser
    , NameRefParser *&_bomRefParser
    , NameRefParser *&_avlRefParser
    , DictionaryStandardParser *&_dictionaryStandardParser
    , DictionaryUserParser *&_dictionaryUserParser
    , DictionaryFontParser *&_dictionaryFontParser
    , DictionaryLineDescParser *&_dictionaryLineDescParser
    , DictionaryFillDescParser *&_dictionaryFillDescParser
    , DictionaryColorParser *&_dictionaryColorParser
    , DictionaryFirmwareParser *&_dictionaryFirmwareParser
):    m_roleRefParser(_roleRefParser)
    , m_functionModeParser(_functionModeParser)
    , m_stepRefParser(_stepRefParser)
    , m_layerRefParser(_layerRefParser)
    , m_bomRefParser(_bomRefParser)
    , m_avlRefParser(_avlRefParser)
    , m_dictionaryStandardParser(_dictionaryStandardParser)
    , m_dictionaryUserParser(_dictionaryUserParser)
    , m_dictionaryFontParser(_dictionaryFontParser)
    , m_dictionaryLineDescParser(_dictionaryLineDescParser)
    , m_dictionaryFillDescParser(_dictionaryFillDescParser)
    , m_dictionaryColorParser(_dictionaryColorParser)
    , m_dictionaryFirmwareParser(_dictionaryFirmwareParser)
{

}

bool ContentParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Content());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("roleRef")))
    {
        data = reader->attributes().value(QStringLiteral("roleRef"));
        if (!m_roleRefParser->parse(reader, data))
            return false;
        m_result->roleRef = m_roleRefParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("roleRef: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("FunctionMode"))
        {
            if (!m_functionModeParser->parse(reader))
                return false;
            auto result = m_functionModeParser->result();
            m_result->functionModeList.append(result);
        }
        else if (name == QStringLiteral("StepRef"))
        {
            if (!m_stepRefParser->parse(reader))
                return false;
            auto result = m_stepRefParser->result();
            m_result->stepRefList.append(result);
        }
        else if (name == QStringLiteral("LayerRef"))
        {
            if (!m_layerRefParser->parse(reader))
                return false;
            auto result = m_layerRefParser->result();
            m_result->layerRefList.append(result);
        }
        else if (name == QStringLiteral("BomRef"))
        {
            if (!m_bomRefParser->parse(reader))
                return false;
            auto result = m_bomRefParser->result();
            m_result->bomRefList.append(result);
        }
        else if (name == QStringLiteral("AvlRef"))
        {
            if (!m_avlRefParser->parse(reader))
                return false;
            auto result = m_avlRefParser->result();
            m_result->avlRefOptional = Optional<NameRef*>(result);
        }
        else if (name == QStringLiteral("DictionaryStandard"))
        {
            if (!m_dictionaryStandardParser->parse(reader))
                return false;
            auto result = m_dictionaryStandardParser->result();
            m_result->dictionaryStandardOptional = Optional<DictionaryStandard*>(result);
        }
        else if (name == QStringLiteral("DictionaryUser"))
        {
            if (!m_dictionaryUserParser->parse(reader))
                return false;
            auto result = m_dictionaryUserParser->result();
            m_result->dictionaryUserOptional = Optional<DictionaryUser*>(result);
        }
        else if (name == QStringLiteral("DictionaryFont"))
        {
            if (!m_dictionaryFontParser->parse(reader))
                return false;
            auto result = m_dictionaryFontParser->result();
            m_result->dictionaryFontOptional = Optional<DictionaryFont*>(result);
        }
        else if (name == QStringLiteral("DictionaryLineDesc"))
        {
            if (!m_dictionaryLineDescParser->parse(reader))
                return false;
            auto result = m_dictionaryLineDescParser->result();
            m_result->dictionaryLineDescOptional = Optional<DictionaryLineDesc*>(result);
        }
        else if (name == QStringLiteral("DictionaryFillDesc"))
        {
            if (!m_dictionaryFillDescParser->parse(reader))
                return false;
            auto result = m_dictionaryFillDescParser->result();
            m_result->dictionaryFillDescOptional = Optional<DictionaryFillDesc*>(result);
        }
        else if (name == QStringLiteral("DictionaryColor"))
        {
            if (!m_dictionaryColorParser->parse(reader))
                return false;
            auto result = m_dictionaryColorParser->result();
            m_result->dictionaryColorOptional = Optional<DictionaryColor*>(result);
        }
        else if (name == QStringLiteral("DictionaryFirmware"))
        {
            if (!m_dictionaryFirmwareParser->parse(reader))
                return false;
            auto result = m_dictionaryFirmwareParser->result();
            m_result->dictionaryFirmwareOptional = Optional<DictionaryFirmware*>(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Content *ContentParser::result()
{
    return m_result.take();
}

}