
#pragma once

namespace Ipc2581b
{

enum class Polarity
{
    Negative,
    Positive,
};

}