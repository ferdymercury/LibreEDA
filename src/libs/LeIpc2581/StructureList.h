
#pragma once

namespace Ipc2581b
{

enum class StructureList
{
    MicrostripEmbedded,
    MicrostripNoMask,
    Other,
    PlaneLessStripline,
    CoplanarWaveguideNoMask,
    MicrostripDualMaskedCovered,
    CoplanarWaveguideMaskCovered,
    CoplanarWaveguideDualMaskedCovered,
    CoplanarWaveguideStripline,
    CoplanarWaveguideEmbedded,
    MicrostripMaskCovered,
    Stripline,
};

}