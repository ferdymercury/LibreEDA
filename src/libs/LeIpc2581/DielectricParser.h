
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Dielectric.h"

namespace Ipc2581b
{

class PropertyParser;
class DielectricListParser;
class QStringParser;

class DielectricParser
{
public:
    DielectricParser(
            DielectricListParser*&
            , QStringParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Dielectric *result();

private:
    DielectricListParser *&m_typeParser;
    QStringParser *&m_commentParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<Dielectric> m_result;
};

}