#include "LayerPadParser.h"


#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

LayerPadParser::LayerPadParser(
){

}

bool LayerPadParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new LayerPad());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

LayerPad *LayerPadParser::result()
{
    return m_result.take();
}

}