
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Outline.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class PolygonParser;

class OutlineParser
{
public:
    OutlineParser(
            PolygonParser*&
            , LineDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Outline *result();

private:
    PolygonParser *&m_polygonParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    QScopedPointer<Outline> m_result;
};

}