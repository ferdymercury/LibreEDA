
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Arc.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class BoolParser;
class DoubleParser;

class ArcParser
{
public:
    ArcParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , BoolParser*&
            , LineDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Arc *result();

private:
    DoubleParser *&m_startXParser;
    DoubleParser *&m_startYParser;
    DoubleParser *&m_endXParser;
    DoubleParser *&m_endYParser;
    DoubleParser *&m_centerXParser;
    DoubleParser *&m_centerYParser;
    BoolParser *&m_clockwiseParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    QScopedPointer<Arc> m_result;
};

}