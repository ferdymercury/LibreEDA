
#pragma once

namespace Ipc2581b
{

enum class PinOneOrientation
{
    LowerLeft,
    UpperCenter,
    LeftCenter,
    Other,
    UpperLeft,
    Left,
};

}