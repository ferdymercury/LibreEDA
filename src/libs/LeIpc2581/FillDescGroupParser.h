
#pragma once

#include <QString>
class QXmlStreamReader;

#include "FillDescGroup.h"

namespace Ipc2581b
{

class FillDescRefParser;
class FillDescParser;

class FillDescGroupParser
{
public:
    FillDescGroupParser(
            FillDescParser*&
            , FillDescRefParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    FillDescGroup *result();

private:
    FillDescParser *&m_fillDescParser;
    FillDescRefParser *&m_fillDescRefParser;
    FillDescGroup *m_result;
};

}