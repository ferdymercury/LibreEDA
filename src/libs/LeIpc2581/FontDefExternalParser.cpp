#include "FontDefExternalParser.h"

#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

FontDefExternalParser::FontDefExternalParser(
    QStringParser *&_nameParser
    , QStringParser *&_urnParser
):    m_nameParser(_nameParser)
    , m_urnParser(_urnParser)
{

}

bool FontDefExternalParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new FontDefExternal());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("urn")))
    {
        data = reader->attributes().value(QStringLiteral("urn"));
        if (!m_urnParser->parse(reader, data))
            return false;
        m_result->urn = m_urnParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("urn: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

FontDefExternal *FontDefExternalParser::result()
{
    return m_result.take();
}

}