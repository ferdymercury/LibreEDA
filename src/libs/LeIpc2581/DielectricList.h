
#pragma once

namespace Ipc2581b
{

enum class DielectricList
{
    DielectricConstant,
    ResinContent,
    ProcessabilityTemp,
    Other,
    LossTangent,
    GlassStyle,
    GlassType,
};

}