#include "RevisionParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

RevisionParser::RevisionParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool RevisionParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, Revision> map =
    {
        {QStringLiteral("B"), Revision::B},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum Revision").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

Revision RevisionParser::result()
{
    return m_result;
}

}