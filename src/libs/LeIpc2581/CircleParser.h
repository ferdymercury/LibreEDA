
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Circle.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class CircleParser
{
public:
    CircleParser(
            DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Circle *result();

private:
    DoubleParser *&m_diameterParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Circle> m_result;
};

}