
#pragma once

#include <QList>
#include "Optional.h"


#include "Simple.h"
#include "Double.h"
#include "QString.h"

namespace Ipc2581b
{

class Glyph
{
public:
	virtual ~Glyph() {}

    QString charCode;
    Double lowerLeftX;
    Double lowerLeftY;
    Double upperRightX;
    Double upperRightY;
    QList<Simple*> simpleList;

};

}