#include "ToolListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ToolListParser::ToolListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ToolListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, ToolList> map =
    {
        {QStringLiteral("ROUTER"), ToolList::Router},
        {QStringLiteral("V_CUTTER"), ToolList::VCutter},
        {QStringLiteral("EXTENSION"), ToolList::Extension},
        {QStringLiteral("LASER"), ToolList::Laser},
        {QStringLiteral("CARBIDE"), ToolList::Carbide},
        {QStringLiteral("FLATNOSE"), ToolList::Flatnose},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum ToolList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

ToolList ToolListParser::result()
{
    return m_result;
}

}