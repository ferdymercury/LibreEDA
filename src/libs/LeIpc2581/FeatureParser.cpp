#include "FeatureParser.h"

#include "StandardShapeParser.h"
#include "UserShapeParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

FeatureParser::FeatureParser(
    StandardShapeParser *&_standardShapeParser
    , UserShapeParser *&_userShapeParser
):    m_standardShapeParser(_standardShapeParser)
    , m_userShapeParser(_userShapeParser)
{

}

bool FeatureParser::parse(QXmlStreamReader *reader)
{
    if (m_standardShapeParser->isSubstitution(reader->name()))
    {
        if(m_standardShapeParser->parse(reader))
        {
            m_result = m_standardShapeParser->result();
            return true;
        }
        else
            return false;
    }
    if (m_userShapeParser->isSubstitution(reader->name()))
    {
        if(m_userShapeParser->parse(reader))
        {
            m_result = m_userShapeParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"Feature\"").arg(reader->name().toString()));
    return false;
}

Feature *FeatureParser::result()
{
    return m_result;
}

bool FeatureParser::isSubstitution(const QStringRef &name) const
{
    if (m_standardShapeParser->isSubstitution(name))
        return true;
    if (m_userShapeParser->isSubstitution(name))
        return true;
    return false;
}

}