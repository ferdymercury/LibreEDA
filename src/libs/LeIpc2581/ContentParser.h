
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Content.h"

namespace Ipc2581b
{

class DictionaryFontParser;
class DictionaryLineDescParser;
class DictionaryFirmwareParser;
class DictionaryColorParser;
class QStringParser;
class NameRefParser;
class DictionaryFillDescParser;
class FunctionModeParser;
class DictionaryStandardParser;
class DictionaryUserParser;

class ContentParser
{
public:
    ContentParser(
            QStringParser*&
            , FunctionModeParser*&
            , NameRefParser*&
            , NameRefParser*&
            , NameRefParser*&
            , NameRefParser*&
            , DictionaryStandardParser*&
            , DictionaryUserParser*&
            , DictionaryFontParser*&
            , DictionaryLineDescParser*&
            , DictionaryFillDescParser*&
            , DictionaryColorParser*&
            , DictionaryFirmwareParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Content *result();

private:
    QStringParser *&m_roleRefParser;
    FunctionModeParser *&m_functionModeParser;
    NameRefParser *&m_stepRefParser;
    NameRefParser *&m_layerRefParser;
    NameRefParser *&m_bomRefParser;
    NameRefParser *&m_avlRefParser;
    DictionaryStandardParser *&m_dictionaryStandardParser;
    DictionaryUserParser *&m_dictionaryUserParser;
    DictionaryFontParser *&m_dictionaryFontParser;
    DictionaryLineDescParser *&m_dictionaryLineDescParser;
    DictionaryFillDescParser *&m_dictionaryFillDescParser;
    DictionaryColorParser *&m_dictionaryColorParser;
    DictionaryFirmwareParser *&m_dictionaryFirmwareParser;
    QScopedPointer<Content> m_result;
};

}