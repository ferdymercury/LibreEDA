
#pragma once

namespace Ipc2581b
{

enum class Mode
{
    Userdef,
    Assembly,
    Fabrication,
    Design,
    Test,
};

}