
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Approval.h"

namespace Ipc2581b
{

class QStringParser;

class ApprovalParser
{
public:
    ApprovalParser(
            QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Approval *result();

private:
    QStringParser *&m_dateTimeParser;
    QStringParser *&m_personRefParser;
    QScopedPointer<Approval> m_result;
};

}