
#pragma once

#include <QList>
#include "Optional.h"


#include "LayerFeature.h"
#include "QString.h"

namespace Ipc2581b
{

class Route
{
public:
	virtual ~Route() {}

    Optional<QString> netOptional;
    QList<LayerFeature*> layerFeatureList;

};

}