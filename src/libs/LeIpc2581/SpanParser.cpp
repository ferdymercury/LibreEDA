#include "SpanParser.h"

#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

SpanParser::SpanParser(
    QStringParser *&_fromLayerParser
    , QStringParser *&_toLayerParser
):    m_fromLayerParser(_fromLayerParser)
    , m_toLayerParser(_toLayerParser)
{

}

bool SpanParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Span());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("fromLayer")))
    {
        data = reader->attributes().value(QStringLiteral("fromLayer"));
        if (!m_fromLayerParser->parse(reader, data))
            return false;
        m_result->fromLayer = m_fromLayerParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("fromLayer: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("toLayer")))
    {
        data = reader->attributes().value(QStringLiteral("toLayer"));
        if (!m_toLayerParser->parse(reader, data))
            return false;
        m_result->toLayer = m_toLayerParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("toLayer: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Span *SpanParser::result()
{
    return m_result.take();
}

}