#include "CadPinParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

CadPinParser::CadPinParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool CadPinParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, CadPin> map =
    {
        {QStringLiteral("BLIND"), CadPin::Blind},
        {QStringLiteral("SURFACE"), CadPin::Surface},
        {QStringLiteral("THRU"), CadPin::Thru},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum CadPin").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

CadPin CadPinParser::result()
{
    return m_result;
}

}