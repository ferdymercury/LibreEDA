#include "ColorGroupParser.h"

#include "ColorParser.h"
#include "ColorRefParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

ColorGroupParser::ColorGroupParser(
    ColorParser *&_colorParser
    , ColorRefParser *&_colorRefParser
):    m_colorParser(_colorParser)
    , m_colorRefParser(_colorRefParser)
{

}

bool ColorGroupParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("Color"))
    {
        if(m_colorParser->parse(reader))
        {
            m_result = m_colorParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("ColorRef"))
    {
        if(m_colorRefParser->parse(reader))
        {
            m_result = m_colorRefParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"ColorGroup\"").arg(reader->name().toString()));
    return false;
}

ColorGroup *ColorGroupParser::result()
{
    return m_result;
}

bool ColorGroupParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("Color"))
        return true;
    if (name == QStringLiteral("ColorRef"))
        return true;
    return false;
}

}