
#pragma once

#include <QList>
#include "Optional.h"


#include "DictionaryFont.h"
#include "DictionaryLineDesc.h"
#include "DictionaryFirmware.h"
#include "DictionaryColor.h"
#include "QString.h"
#include "NameRef.h"
#include "DictionaryFillDesc.h"
#include "FunctionMode.h"
#include "DictionaryStandard.h"
#include "DictionaryUser.h"

namespace Ipc2581b
{

class Content
{
public:
	virtual ~Content() {}

    QString roleRef;
    QList<FunctionMode*> functionModeList;
    QList<NameRef*> stepRefList;
    QList<NameRef*> layerRefList;
    QList<NameRef*> bomRefList;
    Optional<NameRef*> avlRefOptional;
    Optional<DictionaryStandard*> dictionaryStandardOptional;
    Optional<DictionaryUser*> dictionaryUserOptional;
    Optional<DictionaryFont*> dictionaryFontOptional;
    Optional<DictionaryLineDesc*> dictionaryLineDescOptional;
    Optional<DictionaryFillDesc*> dictionaryFillDescOptional;
    Optional<DictionaryColor*> dictionaryColorOptional;
    Optional<DictionaryFirmware*> dictionaryFirmwareOptional;

};

}