
#pragma once

#include <QList>
#include "Optional.h"

#include "FontDef.h"

#include "LineDescGroup.h"
#include "Glyph.h"
#include "QString.h"

namespace Ipc2581b
{

class FontDefEmbedded: public FontDef
{
public:
	virtual ~FontDefEmbedded() {}

    QString name;
    LineDescGroup *lineDescGroup;
    QList<Glyph*> glyphList;

    virtual FontDefType fontDefType() const override
    {
        return FontDefType::FontDefEmbedded;
    }
};

}