
#pragma once

#include <QString>

class QXmlStreamReader;

#include "Units.h"

namespace Ipc2581b
{

class UnitsParser
{
public:
    UnitsParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    Units result();

private:
    Units m_result;
};

}