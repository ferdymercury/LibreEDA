
#pragma once

namespace Ipc2581b
{

enum class PlatingStatus
{
    Via,
    Nonplated,
    Plated,
};

}