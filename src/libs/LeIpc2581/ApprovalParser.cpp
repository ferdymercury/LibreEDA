#include "ApprovalParser.h"

#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

ApprovalParser::ApprovalParser(
    QStringParser *&_dateTimeParser
    , QStringParser *&_personRefParser
):    m_dateTimeParser(_dateTimeParser)
    , m_personRefParser(_personRefParser)
{

}

bool ApprovalParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Approval());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("dateTime")))
    {
        data = reader->attributes().value(QStringLiteral("dateTime"));
        if (!m_dateTimeParser->parse(reader, data))
            return false;
        m_result->dateTime = m_dateTimeParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("dateTime: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("personRef")))
    {
        data = reader->attributes().value(QStringLiteral("personRef"));
        if (!m_personRefParser->parse(reader, data))
            return false;
        m_result->personRef = m_personRefParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("personRef: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Approval *ApprovalParser::result()
{
    return m_result.take();
}

}