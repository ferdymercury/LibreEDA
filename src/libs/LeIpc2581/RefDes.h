
#pragma once

#include <QList>
#include "Optional.h"

#include "BomDes.h"

#include "Bool.h"
#include "QString.h"

namespace Ipc2581b
{

class RefDes: public BomDes
{
public:
	virtual ~RefDes() {}

    QString name;
    Optional<QString> packageRefOptional;
    Optional<Bool> populateOptional;
    QString layerRef;

    virtual BomDesType bomDesType() const override
    {
        return BomDesType::RefDes;
    }
};

}