
#pragma once

namespace Ipc2581b
{

enum class TransmissionList
{
    Other,
    SingleEnded,
    EdgeCoupled,
    BroadsideCoupled,
};

}