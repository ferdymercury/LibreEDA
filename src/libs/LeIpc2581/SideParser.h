
#pragma once

#include <QString>

class QXmlStreamReader;

#include "Side.h"

namespace Ipc2581b
{

class SideParser
{
public:
    SideParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    Side result();

private:
    Side m_result;
};

}