
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "Polygon.h"

namespace Ipc2581b
{

class Contour: public StandardPrimitive
{
public:
	virtual ~Contour() {}

    Polygon *polygon;
    QList<Polygon*> cutoutList;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::Contour;
    }
};

}