
#pragma once

#include <QList>
#include "Optional.h"


#include "Person.h"
#include "Role.h"
#include "Enterprise.h"

namespace Ipc2581b
{

class LogisticHeader
{
public:
	virtual ~LogisticHeader() {}

    QList<Role*> roleList;
    QList<Enterprise*> enterpriseList;
    QList<Person*> personList;

};

}