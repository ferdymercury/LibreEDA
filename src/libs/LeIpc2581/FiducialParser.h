
#pragma once

#include <QString>
class QXmlStreamReader;

#include "Fiducial.h"

namespace Ipc2581b
{

class LocalFiducialParser;
class GoodPanelMarkParser;
class BadBoardMarkParser;
class GlobalFiducialParser;

class FiducialParser
{
public:
    FiducialParser(
            BadBoardMarkParser*&
            , GlobalFiducialParser*&
            , GoodPanelMarkParser*&
            , LocalFiducialParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    Fiducial *result();

private:
    BadBoardMarkParser *&m_badBoardMarkParser;
    GlobalFiducialParser *&m_globalFiducialParser;
    GoodPanelMarkParser *&m_goodPanelMarkParser;
    LocalFiducialParser *&m_localFiducialParser;
    Fiducial *m_result;
};

}