
#pragma once

#include <QList>
#include "Optional.h"

#include "Fiducial.h"

#include "Location.h"
#include "StandardShape.h"
#include "Xform.h"

namespace Ipc2581b
{

class LocalFiducial: public Fiducial
{
public:
	virtual ~LocalFiducial() {}

    Optional<Xform*> xformOptional;
    Location *location;
    StandardShape *standardShape;

    virtual FiducialType fiducialType() const override
    {
        return FiducialType::LocalFiducial;
    }
};

}