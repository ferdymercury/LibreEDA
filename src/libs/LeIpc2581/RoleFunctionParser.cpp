#include "RoleFunctionParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

RoleFunctionParser::RoleFunctionParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool RoleFunctionParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, RoleFunction> map =
    {
        {QStringLiteral("BILLTO"), RoleFunction::Billto},
        {QStringLiteral("BUYER"), RoleFunction::Buyer},
        {QStringLiteral("RECEIVER"), RoleFunction::Receiver},
        {QStringLiteral("OWNER"), RoleFunction::Owner},
        {QStringLiteral("DELIVERTO"), RoleFunction::Deliverto},
        {QStringLiteral("OTHER"), RoleFunction::Other},
        {QStringLiteral("SENDER"), RoleFunction::Sender},
        {QStringLiteral("ENGINEER"), RoleFunction::Engineer},
        {QStringLiteral("DESIGNER"), RoleFunction::Designer},
        {QStringLiteral("CUSTOMERSERVICE"), RoleFunction::Customerservice},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum RoleFunction").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

RoleFunction RoleFunctionParser::result()
{
    return m_result;
}

}