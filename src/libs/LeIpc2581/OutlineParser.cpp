#include "OutlineParser.h"

#include "LineDescGroupParser.h"
#include "PolygonParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

OutlineParser::OutlineParser(
    PolygonParser *&_polygonParser
    , LineDescGroupParser *&_lineDescGroupParser
):    m_polygonParser(_polygonParser)
    , m_lineDescGroupParser(_lineDescGroupParser)
{

}

bool OutlineParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Outline());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Polygon"))
        {
            if (!m_polygonParser->parse(reader))
                return false;
            auto result = m_polygonParser->result();
            m_result->polygon = result;
        }
        else if (m_lineDescGroupParser->isSubstitution(name))
        {
            if (!m_lineDescGroupParser->parse(reader))
                return false;
            auto result = m_lineDescGroupParser->result();
            m_result->lineDescGroup = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Outline *OutlineParser::result()
{
    return m_result.take();
}

}