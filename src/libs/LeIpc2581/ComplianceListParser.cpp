#include "ComplianceListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ComplianceListParser::ComplianceListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ComplianceListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, ComplianceList> map =
    {
        {QStringLiteral("REACH"), ComplianceList::Reach},
        {QStringLiteral("HALOGEN_FREE"), ComplianceList::HalogenFree},
        {QStringLiteral("WEEE"), ComplianceList::Weee},
        {QStringLiteral("ROHS"), ComplianceList::Rohs},
        {QStringLiteral("OTHER"), ComplianceList::Other},
        {QStringLiteral("CONFLICT_MINERALS"), ComplianceList::ConflictMinerals},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum ComplianceList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

ComplianceList ComplianceListParser::result()
{
    return m_result;
}

}