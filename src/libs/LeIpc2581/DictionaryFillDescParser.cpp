#include "DictionaryFillDescParser.h"

#include "UnitsParser.h"
#include "EntryFillDescParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

DictionaryFillDescParser::DictionaryFillDescParser(
    UnitsParser *&_unitsParser
    , EntryFillDescParser *&_entryFillDescParser
):    m_unitsParser(_unitsParser)
    , m_entryFillDescParser(_entryFillDescParser)
{

}

bool DictionaryFillDescParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new DictionaryFillDesc());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("units")))
    {
        data = reader->attributes().value(QStringLiteral("units"));
        if (!m_unitsParser->parse(reader, data))
            return false;
        m_result->units = m_unitsParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("units: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("EntryFillDesc"))
        {
            if (!m_entryFillDescParser->parse(reader))
                return false;
            auto result = m_entryFillDescParser->result();
            m_result->entryFillDescList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

DictionaryFillDesc *DictionaryFillDescParser::result()
{
    return m_result.take();
}

}