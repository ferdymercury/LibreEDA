
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "FontDefEmbedded.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class GlyphParser;
class QStringParser;

class FontDefEmbeddedParser
{
public:
    FontDefEmbeddedParser(
            QStringParser*&
            , LineDescGroupParser*&
            , GlyphParser*&
            );

    bool parse(QXmlStreamReader *reader);
    FontDefEmbedded *result();

private:
    QStringParser *&m_nameParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    GlyphParser *&m_glyphParser;
    QScopedPointer<FontDefEmbedded> m_result;
};

}