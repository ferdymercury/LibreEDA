
#pragma once

#include <QString>

class QXmlStreamReader;

#include "LineEnd.h"

namespace Ipc2581b
{

class LineEndParser
{
public:
    LineEndParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    LineEnd result();

private:
    LineEnd m_result;
};

}