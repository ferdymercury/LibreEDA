
#pragma once


namespace Ipc2581b
{

class BadBoardMark;
class GlobalFiducial;
class GoodPanelMark;
class LocalFiducial;

class Fiducial
{
public:
	virtual ~Fiducial() {}

    enum class FiducialType
    {
        BadBoardMark
        ,GlobalFiducial
        ,GoodPanelMark
        ,LocalFiducial
    };

    virtual FiducialType fiducialType() const = 0;

    inline bool isBadBoardMark() const
    {
        return fiducialType() == FiducialType::BadBoardMark;
    }

    inline BadBoardMark *toBadBoardMark()
    {
        return reinterpret_cast<BadBoardMark*>(this);
    }

    inline const BadBoardMark *toBadBoardMark() const
    {
        return reinterpret_cast<const BadBoardMark*>(this);
    }

    inline bool isGlobalFiducial() const
    {
        return fiducialType() == FiducialType::GlobalFiducial;
    }

    inline GlobalFiducial *toGlobalFiducial()
    {
        return reinterpret_cast<GlobalFiducial*>(this);
    }

    inline const GlobalFiducial *toGlobalFiducial() const
    {
        return reinterpret_cast<const GlobalFiducial*>(this);
    }

    inline bool isGoodPanelMark() const
    {
        return fiducialType() == FiducialType::GoodPanelMark;
    }

    inline GoodPanelMark *toGoodPanelMark()
    {
        return reinterpret_cast<GoodPanelMark*>(this);
    }

    inline const GoodPanelMark *toGoodPanelMark() const
    {
        return reinterpret_cast<const GoodPanelMark*>(this);
    }

    inline bool isLocalFiducial() const
    {
        return fiducialType() == FiducialType::LocalFiducial;
    }

    inline LocalFiducial *toLocalFiducial()
    {
        return reinterpret_cast<LocalFiducial*>(this);
    }

    inline const LocalFiducial *toLocalFiducial() const
    {
        return reinterpret_cast<const LocalFiducial*>(this);
    }


};

}

// For user convenience
#include "BadBoardMark.h"
#include "GlobalFiducial.h"
#include "GoodPanelMark.h"
#include "LocalFiducial.h"
