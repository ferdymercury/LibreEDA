
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Target.h"

namespace Ipc2581b
{

class LocationParser;
class StandardShapeParser;
class XformParser;

class TargetParser
{
public:
    TargetParser(
            XformParser*&
            , LocationParser*&
            , StandardShapeParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Target *result();

private:
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    StandardShapeParser *&m_standardShapeParser;
    QScopedPointer<Target> m_result;
};

}