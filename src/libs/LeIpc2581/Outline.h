
#pragma once

#include <QList>
#include "Optional.h"

#include "Simple.h"

#include "LineDescGroup.h"
#include "Polygon.h"

namespace Ipc2581b
{

class Outline: public Simple
{
public:
	virtual ~Outline() {}

    Polygon *polygon;
    LineDescGroup *lineDescGroup;

    virtual SimpleType simpleType() const override
    {
        return SimpleType::Outline;
    }
};

}