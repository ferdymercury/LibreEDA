
#pragma once

namespace Ipc2581b
{

enum class Exposure
{
    CoveredSecondary,
    Exposed,
    Covered,
    CoveredPrimary,
};

}