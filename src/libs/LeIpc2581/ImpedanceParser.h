
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Impedance.h"

namespace Ipc2581b
{

class TransmissionListParser;
class PropertyParser;
class StructureListParser;
class ImpedanceListParser;
class QStringParser;

class ImpedanceParser
{
public:
    ImpedanceParser(
            ImpedanceListParser*&
            , QStringParser*&
            , TransmissionListParser*&
            , StructureListParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Impedance *result();

private:
    ImpedanceListParser *&m_typeParser;
    QStringParser *&m_commentParser;
    TransmissionListParser *&m_transmissionParser;
    StructureListParser *&m_structureParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<Impedance> m_result;
};

}