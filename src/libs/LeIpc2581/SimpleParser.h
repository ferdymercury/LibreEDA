
#pragma once

#include <QString>
class QXmlStreamReader;

#include "Simple.h"

namespace Ipc2581b
{

class LineParser;
class OutlineParser;
class ArcParser;
class PolylineParser;

class SimpleParser
{
public:
    SimpleParser(
            ArcParser*&
            , LineParser*&
            , OutlineParser*&
            , PolylineParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    Simple *result();

private:
    ArcParser *&m_arcParser;
    LineParser *&m_lineParser;
    OutlineParser *&m_outlineParser;
    PolylineParser *&m_polylineParser;
    Simple *m_result;
};

}