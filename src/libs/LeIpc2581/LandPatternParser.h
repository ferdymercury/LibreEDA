
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "LandPattern.h"

namespace Ipc2581b
{

class PadParser;
class TargetParser;

class LandPatternParser
{
public:
    LandPatternParser(
            PadParser*&
            , TargetParser*&
            );

    bool parse(QXmlStreamReader *reader);
    LandPattern *result();

private:
    PadParser *&m_padParser;
    TargetParser *&m_targetParser;
    QScopedPointer<LandPattern> m_result;
};

}