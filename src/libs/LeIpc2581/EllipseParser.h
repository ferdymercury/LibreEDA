
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Ellipse.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class EllipseParser
{
public:
    EllipseParser(
            DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Ellipse *result();

private:
    DoubleParser *&m_widthParser;
    DoubleParser *&m_heightParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Ellipse> m_result;
};

}