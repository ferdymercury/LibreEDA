
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "QString.h"
#include "Property.h"
#include "GeneralList.h"

namespace Ipc2581b
{

class General: public SpecificationType
{
public:
	virtual ~General() {}

    GeneralList type;
    Optional<QString> commentOptional;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::General;
    }
};

}