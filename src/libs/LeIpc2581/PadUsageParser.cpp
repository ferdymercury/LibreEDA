#include "PadUsageParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

PadUsageParser::PadUsageParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool PadUsageParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, PadUsage> map =
    {
        {QStringLiteral("PLANE"), PadUsage::Plane},
        {QStringLiteral("MASK"), PadUsage::Mask},
        {QStringLiteral("TERMINATION"), PadUsage::Termination},
        {QStringLiteral("NONE"), PadUsage::None},
        {QStringLiteral("TOOLING_HOLE"), PadUsage::ToolingHole},
        {QStringLiteral("VIA"), PadUsage::Via},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum PadUsage").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

PadUsage PadUsageParser::result()
{
    return m_result;
}

}