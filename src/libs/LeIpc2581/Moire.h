
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "Xform.h"
#include "Int.h"
#include "Double.h"

namespace Ipc2581b
{

class Moire: public StandardPrimitive
{
public:
	virtual ~Moire() {}

    Double diameter;
    Double ringWidth;
    Double ringGap;
    Int ringNumber;
    Double lineWidth;
    Double lineLength;
    Double lineAngle;
    Optional<Xform*> xformOptional;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::Moire;
    }
};

}