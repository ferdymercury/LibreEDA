
#pragma once

#include <QList>
#include "Optional.h"

#include "BomDes.h"

#include "QString.h"

namespace Ipc2581b
{

class MatDes: public BomDes
{
public:
	virtual ~MatDes() {}

    QString name;
    Optional<QString> layerRefOptional;

    virtual BomDesType bomDesType() const override
    {
        return BomDesType::MatDes;
    }
};

}