
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Temperature.h"

namespace Ipc2581b
{

class TemperatureListParser;
class PropertyParser;
class QStringParser;

class TemperatureParser
{
public:
    TemperatureParser(
            TemperatureListParser*&
            , QStringParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Temperature *result();

private:
    TemperatureListParser *&m_typeParser;
    QStringParser *&m_commentParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<Temperature> m_result;
};

}