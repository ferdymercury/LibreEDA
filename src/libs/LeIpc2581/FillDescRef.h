
#pragma once

#include <QList>
#include "Optional.h"

#include "FillDescGroup.h"

#include "QString.h"

namespace Ipc2581b
{

class FillDescRef: public FillDescGroup
{
public:
	virtual ~FillDescRef() {}

    QString id;

    virtual FillDescGroupType fillDescGroupType() const override
    {
        return FillDescGroupType::FillDescRef;
    }
};

}