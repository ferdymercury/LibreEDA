
#pragma once

#include <QList>
#include "Optional.h"

#include "UserPrimitive.h"

#include "ColorGroup.h"
#include "BoundingBox.h"
#include "Xform.h"
#include "QString.h"
#include "FontRef.h"
#include "Int.h"

namespace Ipc2581b
{

class Text: public UserPrimitive
{
public:
	virtual ~Text() {}

    QString textString;
    Int fontSize;
    Optional<Xform*> xformOptional;
    BoundingBox *boundingBox;
    FontRef *fontRef;
    ColorGroup *colorGroup;

    virtual UserPrimitiveType userPrimitiveType() const override
    {
        return UserPrimitiveType::Text;
    }
};

}