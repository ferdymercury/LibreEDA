
#pragma once

#include <QString>

class QXmlStreamReader;

#include "PadUse.h"

namespace Ipc2581b
{

class PadUseParser
{
public:
    PadUseParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    PadUse result();

private:
    PadUse m_result;
};

}