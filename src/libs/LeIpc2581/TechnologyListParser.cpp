#include "TechnologyListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

TechnologyListParser::TechnologyListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool TechnologyListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, TechnologyList> map =
    {
        {QStringLiteral("HDI"), TechnologyList::Hdi},
        {QStringLiteral("EMBEDDED_COMPONENT"), TechnologyList::EmbeddedComponent},
        {QStringLiteral("FLEX"), TechnologyList::Flex},
        {QStringLiteral("RIGID"), TechnologyList::Rigid},
        {QStringLiteral("OTHER"), TechnologyList::Other},
        {QStringLiteral("RIGID_FLEX"), TechnologyList::RigidFlex},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum TechnologyList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

TechnologyList TechnologyListParser::result()
{
    return m_result;
}

}