
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Route.h"

namespace Ipc2581b
{

class LayerFeatureParser;
class QStringParser;

class RouteParser
{
public:
    RouteParser(
            QStringParser*&
            , LayerFeatureParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Route *result();

private:
    QStringParser *&m_netParser;
    LayerFeatureParser *&m_layerFeatureParser;
    QScopedPointer<Route> m_result;
};

}