#include "SpecificationTypeParser.h"

#include "DielectricParser.h"
#include "BackdrillParser.h"
#include "ComplianceParser.h"
#include "V_CutParser.h"
#include "ToolParser.h"
#include "ImpedanceParser.h"
#include "ConductorParser.h"
#include "GeneralParser.h"
#include "TechnologyParser.h"
#include "TemperatureParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

SpecificationTypeParser::SpecificationTypeParser(
    BackdrillParser *&_backdrillParser
    , ComplianceParser *&_complianceParser
    , ConductorParser *&_conductorParser
    , DielectricParser *&_dielectricParser
    , GeneralParser *&_generalParser
    , ImpedanceParser *&_impedanceParser
    , TechnologyParser *&_technologyParser
    , TemperatureParser *&_temperatureParser
    , ToolParser *&_toolParser
    , V_CutParser *&_v_CutParser
):    m_backdrillParser(_backdrillParser)
    , m_complianceParser(_complianceParser)
    , m_conductorParser(_conductorParser)
    , m_dielectricParser(_dielectricParser)
    , m_generalParser(_generalParser)
    , m_impedanceParser(_impedanceParser)
    , m_technologyParser(_technologyParser)
    , m_temperatureParser(_temperatureParser)
    , m_toolParser(_toolParser)
    , m_v_CutParser(_v_CutParser)
{

}

bool SpecificationTypeParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("Backdrill"))
    {
        if(m_backdrillParser->parse(reader))
        {
            m_result = m_backdrillParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Compliance"))
    {
        if(m_complianceParser->parse(reader))
        {
            m_result = m_complianceParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Conductor"))
    {
        if(m_conductorParser->parse(reader))
        {
            m_result = m_conductorParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Dielectric"))
    {
        if(m_dielectricParser->parse(reader))
        {
            m_result = m_dielectricParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("General"))
    {
        if(m_generalParser->parse(reader))
        {
            m_result = m_generalParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Impedance"))
    {
        if(m_impedanceParser->parse(reader))
        {
            m_result = m_impedanceParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Technology"))
    {
        if(m_technologyParser->parse(reader))
        {
            m_result = m_technologyParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Temperature"))
    {
        if(m_temperatureParser->parse(reader))
        {
            m_result = m_temperatureParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Tool"))
    {
        if(m_toolParser->parse(reader))
        {
            m_result = m_toolParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("V_Cut"))
    {
        if(m_v_CutParser->parse(reader))
        {
            m_result = m_v_CutParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"SpecificationType\"").arg(reader->name().toString()));
    return false;
}

SpecificationType *SpecificationTypeParser::result()
{
    return m_result;
}

bool SpecificationTypeParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("Backdrill"))
        return true;
    if (name == QStringLiteral("Compliance"))
        return true;
    if (name == QStringLiteral("Conductor"))
        return true;
    if (name == QStringLiteral("Dielectric"))
        return true;
    if (name == QStringLiteral("General"))
        return true;
    if (name == QStringLiteral("Impedance"))
        return true;
    if (name == QStringLiteral("Technology"))
        return true;
    if (name == QStringLiteral("Temperature"))
        return true;
    if (name == QStringLiteral("Tool"))
        return true;
    if (name == QStringLiteral("V_Cut"))
        return true;
    return false;
}

}