import qbs 1.0

LedaLibrary {
    name: "LeGraphicsView"

    Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    Depends { name: "Core"; }

    cpp.defines: base.concat(["LEGRAPHICSVIEW_LIBRARY"])

    files: [
        "LeGraphicsFeatureOption.cpp",
        "LeGraphicsFeatureOption.h",
        "LeGraphics.h",
        "LeGraphicsHandleItem.cpp",
        "LeGraphicsHandleItem.h",
        "LeGraphicsItem.cpp",
        "LeGraphicsItem.h",
        "LeGraphicsItemLayer.cpp",
        "LeGraphicsItemLayer.h",
        "LeGraphicsLayerStackModel.cpp",
        "LeGraphicsLayerStackModel.h",
        "LeGraphicsPalette.cpp",
        "LeGraphicsPalette.h",
        "LeGraphicsRulerBar.cpp",
        "LeGraphicsRulerBar.h",
        "LeGraphicsRuler.cpp",
        "LeGraphicsRuler.h",
        "LeGraphicsScene.cpp",
        "LeGraphicsSceneEventFilter.cpp",
        "LeGraphicsSceneEventFilter.h",
        "LeGraphicsScene.h",
        "LeGraphicsStyle.cpp",
        "LeGraphicsStyle.h",
        "LeGraphicsView.cpp",
        "LeGraphicsView.h",
        "Solarized.cpp",
        "Solarized.h",
    ]

    Export {
        Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    }
}
