INCLUDEPATH += $$PWD/..

win32:CONFIG(release, debug|release): LIBS += -L$$top_builddir/src/lib/LeGraphicsView/release/ -lLeGraphicsView
else:win32:CONFIG(debug, debug|release): LIBS += -L$$top_builddir/src/lib/LeGraphicsView/debug/ -lLeGraphicsView
else:unix: LIBS += -L$$top_builddir/src/lib/LeGraphicsView/ -lLeGraphicsView

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeGraphicsView/release/libLeGraphicsView.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeGraphicsView/debug/libLeGraphicsView.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeGraphicsView/release/LeGraphicsView.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeGraphicsView/debug/LeGraphicsView.lib
else:unix: PRE_TARGETDEPS += $$top_builddir/src/lib/LeGraphicsView/libLeGraphicsView.a
