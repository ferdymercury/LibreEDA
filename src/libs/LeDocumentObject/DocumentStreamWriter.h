#pragma once

#include "LeDocumentObject_global.h"

#include <QObject>

class QIODevice;
class QXmlStreamWriter;

class TestDocumentStreamWriter;

namespace LDO
{

    class Document;
    class IDocumentObject;

    class LDO_EXPORT DocumentStreamWriter : public QObject
    {
        Q_OBJECT
        friend class ::TestDocumentStreamWriter;

    public:
        explicit DocumentStreamWriter(QObject *parent = nullptr);

        void setAutoFormatting(bool enabled);

        void setDevice(QIODevice *device);
        QIODevice *device() const;

        bool writeDocument(const Document *document);
        QString errorString() const;

    private:
        void writeEnumValue(const QMetaProperty &metaProperty, const QVariant &variantValue);
        void writeFlagValue(const QMetaProperty &metaProperty, const QVariant &variantValue);
        void writeUserTypeValue(const QMetaProperty &metaProperty, const QVariant &variantValue);
        void writeStandardTypeValue(const QMetaProperty &metaProperty, const QVariant &variant);
        void writeDocumentObject(const IDocumentObject *object);
        void writeDocumentObject(const QMetaObject *metaObject, const QObject *object);
        void writeText(const QString &text);
        void writeProperty(const QMetaProperty &metaProperty, const QVariant &variantValue);
        QString pointerTypeNameForClass(const QString &className);

    private:
        QXmlStreamWriter *m_writer = nullptr;
    };

}
