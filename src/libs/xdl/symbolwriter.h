#pragma once

#include "xdl_global.h"

#include <QString>

class QIODevice;

namespace SymbolEditor
{

    class Symbol;
    struct WriterPrivate;

    class XDL_EXPORT Writer
    {
    public:
        Writer();
        ~Writer();

        bool write(const QString &filename, const Symbol *symbol);

        bool write(QIODevice *device, const Symbol *symbol);

        QString errorString() const;

        void setAutoFormating(bool autoFormating);

    private:
        WriterPrivate *p;
        QString m_errorString;
    };

}
