import qbs 1.0

LedaLibrary {
    name: "Xdl"
    cpp.defines: base.concat([
                                 "XDL_LIBRARY",
                             ])

    // QtGui only needed for QIcon in Item::Icon()
    Depends { name: "Qt"; submodules: ["core", "gui"] }

    files: [
        "leda-xdl-symbol.xsd",
        "symbol.cpp",
        "symbol.h",
        "symbolreader.cpp",
        "symbolreader.h",
        "symbolwriter.cpp",
        "symbolwriter.h",
        "xdl_global.h",
    ]
}
