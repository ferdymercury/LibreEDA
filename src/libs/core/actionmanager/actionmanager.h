#ifndef ACTIONMANAGER_H
#define ACTIONMANAGER_H

#include "core_global.h"

#include <QObject>

class CORE_EXPORT ActionManager : public QObject
{
    Q_OBJECT
public:
    explicit ActionManager(QObject *parent = nullptr);
    ~ActionManager();

signals:

public slots:
};

#endif // ACTIONMANAGER_H
