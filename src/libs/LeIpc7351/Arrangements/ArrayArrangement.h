#pragma once

#include "LeIpc7351/Arrangement.h"

#include <QSizeF>

class ArrayArrangement : public Arrangement
{
    Q_OBJECT

    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(int rowCount READ rowCount WRITE setRowCount NOTIFY rowCountChanged)
    Q_PROPERTY(int columnCount READ columnCount WRITE setColumnCount NOTIFY columnCountChanged)
    Q_PROPERTY(qreal rowPitch READ rowPitch WRITE setRowPitch NOTIFY rowPitchChanged)
    Q_PROPERTY(qreal columnPitch READ columnPitch WRITE setColumnPitch NOTIFY columnPitchChanged)
    Q_PROPERTY(bool staggered READ isStaggered WRITE setStaggered NOTIFY staggeredChanged)

public:
    ArrayArrangement();
    ~ArrayArrangement();

    QSizeF size() const;
    int rowCount() const;
    int columnCount() const;
    qreal rowPitch() const;
    qreal columnPitch() const;
    bool isStaggered() const;

signals:
    void arrangementChanged();
    void sizeChanged(QSizeF size);
    void rowCountChanged(int rowCount);
    void columnCountChanged(int columnCount);
    void columnPitchChanged(qreal columnPitch);
    void rowPitchChanged(qreal rowPitch);
    void staggeredChanged(bool staggered);

public slots:
    void setSize(QSizeF size);
    void setRowCount(int rowCount);
    void setColumnCount(int columnCount);
    void setRowPitch(qreal rowPitch);
    void setColumnPitch(qreal columnPitch);
    void setStaggered(bool staggered);

private:
    QSizeF m_size;
    int m_rowCount;
    int m_columnCount;
    qreal m_rowPitch;
    qreal m_columnPitch;
    bool m_staggered;

    // Arrangement interface
protected:
    virtual void calculate() override;
};
