#pragma once

#include "LeIpc7351/Arrangement.h"

#include <QObject>
#include <QSizeF>

// TODO: Have a Feature property, this way we can use arrangement on any sort of feature
// or maybe a IDocumentObject* of just a QUuid

class QuadInLineArrangement: public Arrangement
{
    Q_OBJECT

    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(int terminalCount READ terminalCount WRITE setTerminalCount NOTIFY terminalCountChanged)
    Q_PROPERTY(qreal terminalPitch READ terminalPitch WRITE setTerminalPitch NOTIFY terminalPitchChanged)

public:
    enum
    {
        Type = ArrangementBaseType + 1
    };

    QuadInLineArrangement();
    ~QuadInLineArrangement();

    QSizeF size() const;
    int terminalCount() const;
    qreal terminalPitch() const;

public slots:
    void setSize(QSizeF size);
    void setTerminalCount(int terminalCount);
    void setTerminalPitch(qreal terminalPitch);

signals:
    void sizeChanged(QSizeF size);
    void terminalCountChanged(int terminalCount);
    void terminalPitchChanged(qreal terminalPitch);

private:
    QSizeF m_size;
    int m_terminalCount;
    qreal m_terminalPitch;

    // IDocumentObject interface
public:
    virtual int objectType() const override { return Type; }

    // Arrangement interface
protected:
    virtual void calculate() override;
};
