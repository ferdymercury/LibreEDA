#include "Arrangement.h"


Arrangement::Arrangement()
    : GeoFeature()
{
}

Arrangement::~Arrangement()
{
}

QString Arrangement::namingPrefix() const
{
    return m_namingPrefix;
}

L7::NamingScheme Arrangement::namingScheme() const
{
    return m_namingScheme;
}

QList<Transform> Arrangement::transformations() const
{
    return m_transformations;
}

QList<QString> Arrangement::names() const
{
    return m_names;
}

void Arrangement::setNamingPrefix(QString namingPrefix)
{
    if (m_namingPrefix == namingPrefix)
        return;

    _IDO_SET_PROPERTY(namingPrefix);
}

void Arrangement::setNamingScheme(L7::NamingScheme namingScheme)
{
    if (m_namingScheme == namingScheme)
        return;

    _IDO_SET_PROPERTY(namingScheme);
}

void Arrangement::setTransformations(const QList<Transform> &transformations)
{
    if (m_transformations == transformations)
        return;

    _IDO_SET_PROPERTY(transformations);
}
