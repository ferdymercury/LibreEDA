#include "ButterflyPrimitive.h"


ButterflyPrimitive::ButterflyPrimitive()
    : Primitive()
{

}

ButterflyPrimitive::~ButterflyPrimitive()
{

}

qreal ButterflyPrimitive::size() const
{
    return m_size;
}

ButterflyPrimitive::Shape ButterflyPrimitive::shape() const
{
    return m_shape;
}

void ButterflyPrimitive::setSize(qreal size)
{
    if (qFuzzyCompare(m_size, size))
        return;

    _IDO_SET_PROPERTY(size);
}

void ButterflyPrimitive::setShape(ButterflyPrimitive::Shape shape)
{
    if (m_shape == shape)
        return;

    _IDO_SET_PROPERTY(shape);
}

