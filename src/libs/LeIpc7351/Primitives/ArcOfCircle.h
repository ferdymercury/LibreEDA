#pragma once

#include "LeIpc7351/Primitive.h"


class ArcOfCircle : public Primitive
{
    Q_OBJECT

    Q_PROPERTY(qreal diameter READ diameter WRITE setDiameter NOTIFY diameterChanged)
    Q_PROPERTY(qreal startAngle READ startAngle WRITE setStartAngle NOTIFY startAngleChanged)
    Q_PROPERTY(qreal spanAngle READ spanAngle WRITE setSpanAngle NOTIFY spanAngleChanged)

    qreal m_spanAngle = 360.0;
    qreal m_startAngle = 0.0;
    qreal m_diameter = 0.0;

public:
    ArcOfCircle();
    ~ArcOfCircle();

    qreal spanAngle() const;
    qreal startAngle() const;
    qreal diameter() const;

signals:
    void spanAngleChanged(qreal spanAngle);
    void startAngleChanged(qreal startAngle);
    void diameterChanged(qreal diameter);

public slots:
    void setSpanAngle(qreal spanAngle);
    void setStartAngle(qreal startAngle);
    void setDiameter(qreal diameter);
};

