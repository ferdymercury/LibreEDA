#include "RegularPolygonPrimitive.h"


RegularPolygonPrimitive::RegularPolygonPrimitive()
    : Primitive()
{
}
RegularPolygonPrimitive::~RegularPolygonPrimitive()
{
}

qreal RegularPolygonPrimitive::diameter() const
{
    return m_diameter;
}

int RegularPolygonPrimitive::sideCount() const
{
    return m_sideCount;
}

void RegularPolygonPrimitive::setDiameter(qreal diameter)
{
    if (qFuzzyCompare(m_diameter, diameter))
        return;

    _IDO_SET_PROPERTY(diameter);
}

void RegularPolygonPrimitive::setSideCount(int sideCount)
{
    if (m_sideCount == sideCount)
        return;

    if (sideCount < 3)
        return;

    if (sideCount > 12)
        return;

    _IDO_SET_PROPERTY(sideCount);
}
