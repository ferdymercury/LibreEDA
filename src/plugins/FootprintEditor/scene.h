
#pragma once

#include "OldGraphicsView/Scene.h"

#include <QRectF>
#include <QSize>

namespace FootprintEditor
{

    class Scene : public LeGraphicsView::Scene
    {
        Q_OBJECT
    public:
        explicit Scene(QObject *parent = nullptr);
        explicit Scene(const QRectF &sceneRect, QObject *parent = nullptr);
        explicit Scene(qreal x, qreal y, qreal width, qreal height,
                       QObject *parent = nullptr);

    public slots:

    protected:
        //void drawBackground(QPainter *painter, const QRectF &rect);

    private:
        void init();

        QPointF snapScenePos(QPointF pos);

        // The size of the cells in the grid.
        QSize m_cellSize;
    };

}