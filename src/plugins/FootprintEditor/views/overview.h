
#pragma once

#include "OldGraphicsView/View.h"

namespace FootprintEditor
{

    class OverView : public LeGraphicsView::View
    {
        Q_OBJECT
    public:
        explicit OverView(QWidget *parent = nullptr);

        void setObservedView(LeGraphicsView::View *view);

    signals:

    protected slots:
        void updateObservedRect();
        void synchroniseSettings();

    protected:
        void drawForeground(QPainter *painter, const QRectF &rect);
        void resizeEvent(QResizeEvent *event);

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void mouseReleaseEvent(QMouseEvent *event);

        bool eventFilter(QObject *obj, QEvent *ev);

        void forceRedraw();

    private:
        enum Move
        {
            NoMove = 0,
            MoveRect,
            MoveTopLeft,
            MoveTopRight,
            MoveBottomRight,
            MoveBottomLeft
        };
        LeGraphicsView::View *m_observedView;
        QRectF m_observedRect;
        Move m_move;
        QPointF m_lastPos;

        // QWidget interface
    protected:
        void showEvent(QShowEvent *event);
    };

}
