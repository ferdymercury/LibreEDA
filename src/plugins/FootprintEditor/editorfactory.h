
#pragma once

#include "footprinteditor_global.h"

#include "core/editormanager/ieditorfactory.h"

namespace FootprintEditor
{

    class FOOTPRINTEDITOR_EXPORT PcbEditorFactory : public IEditorFactory
    {
        Q_OBJECT

    public:
        explicit PcbEditorFactory(QObject *parent = nullptr);
        ~PcbEditorFactory();

        // IEditorFactory interface
    public:
        virtual IEditor *createEditor();
    };

}