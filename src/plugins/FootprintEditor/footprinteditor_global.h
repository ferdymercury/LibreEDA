#pragma once

#include <qglobal.h>

#if defined(FOOTPRINTEDITOR_LIBRARY)
#  define FOOTPRINTEDITOR_EXPORT Q_DECL_EXPORT
#else
#  define FOOTPRINTEDITOR_EXPORT Q_DECL_IMPORT
#endif

