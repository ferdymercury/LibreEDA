#pragma once

#include "GerberViewer.h"
#include "core/editormanager/ieditorfactory.h"

namespace GerberViewer
{
    class Editor;

    class GERBERVIEWER_EXPORT EditorFactory : public IEditorFactory
    {
        Q_OBJECT
    public:
        explicit EditorFactory(QObject *parent = nullptr);

    signals:

    public slots:
        void applySettings();

    private:
        QList<Editor *> m_editors;

        // IEditorFactory interface
    public:
        virtual IEditor *createEditor();
    };
}
