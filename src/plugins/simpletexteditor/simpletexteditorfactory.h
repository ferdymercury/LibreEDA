#ifndef SIMPLETEXTEDITORFACTORY_H
#define SIMPLETEXTEDITORFACTORY_H

#include "simpletexteditor_global.h"

#include "core/editormanager/ieditorfactory.h"

class SIMPLETEXTEDITOR_EXPORT SimpleTextEditorFactory : public IEditorFactory
{
    Q_OBJECT
public:
    explicit SimpleTextEditorFactory(QObject *parent = nullptr);

signals:

public slots:

    // IEditorFactory interface
public:
    virtual IEditor *createEditor();
};

#endif // SIMPLETEXTEDITORFACTORY_H
