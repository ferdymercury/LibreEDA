#pragma once

#include "abstractsnapstrategy.h"

namespace SymbolEditor
{

    class SnapToItemEndPointStrategy: public AbstractSnapStrategy
    {
    public:
        explicit SnapToItemEndPointStrategy(QObject *parent = nullptr);

        // AbstractSnapStrategy interface
    public:
        SnapResult snap(QPointF mousePos, qreal maxDistance) override;
    };

}
