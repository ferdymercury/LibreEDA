#include "placeellipsetool.h"
#include "item/ellipseitem.h"
#include "command/additemcommand.h"
#include "view/scene.h"

#include <QGraphicsSceneMouseEvent>
#include <QLineF>

namespace SymbolEditor
{

    PlaceEllipseTool::PlaceEllipseTool(QObject *parent):
        AbstractTool(parent),
        m_state(WaitingForActivation),
        m_item(nullptr)
    {
        setLabel("<b>P</b>lace a <b>E</b>llipse");
        setShortcut(QKeySequence("p,e"));
        setIcon(QIcon::fromTheme("draw-ellipse"));
        updateAction();
    }

    PlaceEllipseTool::~PlaceEllipseTool()
    {
    }

    void PlaceEllipseTool::addItem()
    {
        m_item = new GraphicsEllipseItem();
        m_item->setEnabled(false);
        scene()->addItem(m_item);
    }

    void PlaceEllipseTool::updateItem()
    {
        m_item->setProperty(PositionProperty, m_center);
        m_item->setProperty(XRadiusProperty, QLineF(m_center, m_xRadiusPos).length());
        m_item->setProperty(YRadiusProperty, QLineF(m_center, m_yRadiusPos).length());
    }

    void PlaceEllipseTool::removeItem()
    {
        scene()->removeItem(m_item);
        delete m_item;
    }

    bool PlaceEllipseTool::mouseClickEvent(QGraphicsSceneMouseEvent *event)
    {
        switch (m_state)
        {
            case WaitingForActivation:
                break;
            case WaitingForCenter:
            {
                m_center = m_xRadiusPos = m_yRadiusPos = event->scenePos();
                addItem();
                updateItem();
                m_state = WaitingForXRadius;
                break;
            }
            case WaitingForXRadius:
            {
                m_xRadiusPos = m_yRadiusPos = event->scenePos();
                updateItem();
                m_state = WaitingForYRadius;
                break;
            }
            case WaitingForYRadius:
            {
                m_yRadiusPos = event->scenePos();
                updateItem();
                m_item->setEnabled(true);

                auto command = new AddItemCommand();
                command->setItemType(Ellipse); // FIXME: typeForDocument()
                command->setItemProperties(m_item->properties());
                appendCommand(command);

                removeItem();
                m_state = WaitingForCenter;
                break;
            }
        }
        return true;
    }

    bool PlaceEllipseTool::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
    {
        switch (m_state)
        {
            case WaitingForActivation:
                break;
            case WaitingForCenter:
                break;
            case WaitingForXRadius:
                m_xRadiusPos = m_yRadiusPos = event->scenePos();
                updateItem();
                break;
            case WaitingForYRadius:
                m_yRadiusPos = event->scenePos();
                updateItem();
                break;
        }
        return true;
    }

    void PlaceEllipseTool::activate(Scene *scene)
    {
        if (m_state != WaitingForActivation)
        {
            desactivate();
        }
        setScene(scene);
        m_state = WaitingForCenter;
    }

    void PlaceEllipseTool::desactivate()
    {
        switch (m_state)
        {
            case WaitingForActivation:
            case WaitingForCenter:
                break;
            case WaitingForXRadius:
            case WaitingForYRadius:
                removeItem();
                break;
        }
        setScene(nullptr);
        m_state = WaitingForActivation;
    }

}
