#pragma once

#include "tool.h"

#include <QPointF>

class QGraphicsRectItem;

namespace SymbolEditor
{
    class GraphicsCircleItem;

    class PlaceCircleTool : public GraphicsTool
    {
        Q_OBJECT

    public:
        PlaceCircleTool(QObject *parent = nullptr);
        ~PlaceCircleTool();

    private:
        enum State
        {
            WaitingForActivation,
            WaitingForCenter,
            WaitingForRadius,
        };
        State m_state;
        QPointF m_p1;
        QPointF m_p2;
        GraphicsCircleItem *m_item;

        void updateItem();

        // GraphicsTool interface
    public:
        void activate(QGraphicsScene *scene) override;
        void desactivate() override;

    protected:
        bool mouseClickEvent(QGraphicsSceneMouseEvent *event) override;
        bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
        bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

    };

} // namespace SymbolEditor

