#pragma once

#include "tool/abstracttool.h"

#include <QPointF>

class QGraphicsItem;

namespace SymbolEditor
{


    class CloneItemTool : public AbstractTool
    {
    public:
        CloneItemTool(QObject *parent = nullptr);

    private:
        enum State
        {
            WaitForActivation,
            WaitForOrigin,
            WaitForDestination
        };
        State m_state;
        QList<quint64> m_itemIdList;
        QList<QGraphicsItem *> m_clones;
        QPointF m_origin;
        QPointF m_destination;

        QGraphicsItem *cloneItem(QGraphicsItem *item);
        void addGraphicalEffect(QGraphicsItem *item);
        void addClones();
        void moveClones(const QPointF &pos);
        void removeClones();
        void pushCommand();

        // GraphicsTool interface
    public:
        void activate(Scene *scene) override;
        void desactivate() override;

        // GraphicsTool interface
    protected:
        bool mouseClickEvent(QGraphicsSceneMouseEvent *event) override;
        bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    };

}
