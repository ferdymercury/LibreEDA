#pragma once

#include "tool/abstracttool.h"

#include <QItemSelection>

class QToolBar;

namespace SymbolEditor
{

    class Item;
    class ItemPropertyEditor;
    class ObjectInspectorView;
    class ObjectInspectorModel;

    class SelectTool : public AbstractTool
    {
        Q_OBJECT

    public:
        explicit SelectTool(QObject *parent = nullptr);
        ~SelectTool();


    public slots:
        void addDocumentItem(quint64 id, const Item *item);
        void updateDocumentItem(quint64 id, const Item *item);
        void updateDocumentItemProperty(quint64 itemId, quint64 propertyId, const QVariant &value);
        void removeDocumentItem(quint64 id);

    private slots:
        void onObjectInspectorVisibilityChangeRequested(quint64 id, bool visibility);
        void onObjectInspectorLockStateChangeRequested(quint64 id, bool lockState);
        void onObjectInspectorSelectionChanged(const QItemSelection &selected,
                                               const QItemSelection &deselected);
        void onSceneSelectionChanged();
        void onArrangeActionTriggered();

    private:
        void setupObjectInspector();
        void setupPropertyBrowser();
        void setupSubTools();
        void setupArrangeTools();
        void initStateMachine();

        // TODO: Move these 2 to ObjectInspectorWidget (SymbolEditor specific)
        ObjectInspectorModel *m_objectInspectorModel;
        ObjectInspectorView *m_objectInspectorView;
        ItemPropertyEditor *m_propertyEditor;

        QWidget *m_arrangeWidget;
        QList<QAction*> m_arrangeActions;
        QToolBar *m_arrangeHToolBar;
        QToolBar *m_arrangeVToolBar;
        QAction *createArrangeAction(Qt::Alignment alignment, const QString &iconName,
                                     const QString &text, const QString &shortcut);
        QAction *createDistributeAction(Distribution distribution, qreal distance,
                                        const QString &iconName, const QString &text, const QString &shortcut);
        QAction *createStackOrderAction(StackOrderOperation operation, const QString &iconName,
                                        const QString &text, const QString &shortcut);

        bool m_changingSelection = false;

        // GraphicsTool interface
    public:
        void activate(Scene *Scene) override;
        void desactivate() override;
        /*
        void mousePressEvent(QMouseEvent *event) override;
        void mouseMoveEvent(QMouseEvent *event) override;
        void mouseReleaseEvent(QMouseEvent *event) override;
        void keyPressEvent(QKeyEvent *event) override;
        void keyReleaseEvent(QKeyEvent *event) override;
        */
    };

}
