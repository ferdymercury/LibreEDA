#pragma once

#include "tool/interactivetool.h"
#include "item/item.h"

#include <QPointF>
#include <QString>

class QWidget;
class QAction;

namespace SymbolEditor
{

    class ToolPropertyEditor;
    class AddItemCommand;

    // TBD: ctrl+space: cycle mode
    // Use template whith GraphicsXyzItem, or use GraphicsObject base class
    // Should work as well for single point (either a "Point" object, or a fixed shape/size object)
    // addPoint return a GraphicsHandle, movePoint has a default impl.
    class PlacementTool : public InteractiveTool
    {
        Q_OBJECT

        Q_PROPERTY(SymbolEditor::LineStyle lineStyle READ lineStyle WRITE setLineStyle NOTIFY lineStyleChanged)
        Q_PROPERTY(SymbolEditor::LineWidth lineWidth READ lineWidth WRITE setLineWidth NOTIFY lineWidthChanged)
        Q_PROPERTY(SymbolEditor::Color lineColor READ lineColor WRITE setLineColor NOTIFY lineColorChanged)
        Q_PROPERTY(SymbolEditor::FillStyle fillStyle READ fillStyle WRITE setFillStyle NOTIFY fillStyleChanged)
        Q_PROPERTY(SymbolEditor::Color fillColor READ fillColor WRITE setFillColor NOTIFY fillColorChanged)

    public:
        explicit PlacementTool(QObject *parent = nullptr);
        ~PlacementTool();

        // Typ. mouse clicked, idx == 0
        virtual GraphicsItem *beginInsert(const QPointF &pos) = 0;
        // Typ. first mouse move after mouse clicked, idx > 0
        virtual void addPoint(int idx, const QPointF &pos) = 0;
        // Typ. mouse clicked, idx >= 0
        virtual void freezePoint(int idx, const QPointF &pos) = 0;
        // Typ. Esc pressed, idx > 0
        virtual bool removePoint(int idx, const QPointF &pos) = 0;
        // Typ. mouse move, idx > 0
        virtual void movePoint(int idx, const QPointF &pos) = 0;
        // Typ. mouse doubleclick
        virtual void endInsert(const QPointF &pos) = 0;
        // Typ. Esc pressed, idx > 0
        virtual void cancelInsert() = 0;

        LineStyle lineStyle() const;
        LineWidth lineWidth() const;
        Color lineColor() const;
        FillStyle fillStyle() const;
        Color fillColor() const;

    public slots:
        void goBack();
        void resetTool();

        void setLineStyle(LineStyle lineStyle);
        void setLineWidth(LineWidth lineWidth);
        void setLineColor(Color lineColor);
        void setFillStyle(FillStyle fillStyle);
        void setFillColor(Color fillColor);

    signals:
        void lineStyleChanged(LineStyle lineStyle);
        void lineWidthChanged(LineWidth lineWidth);
        void lineColorChanged(Color lineColor);
        void fillStyleChanged(FillStyle fillStyle);
        void fillColorChanged(Color fillColor);

    private:
        int m_index;
        QPointF m_pressPos;
        QPointF m_movePos;
        bool m_addPointOnMouseMove;
        bool m_isActive;
        GraphicsItem *m_item;
        void setItem(GraphicsItem *item);
        Palette m_palette;
        LineStyle m_lineStyle;
        LineWidth m_lineWidth;
        Color m_lineColor;
        FillStyle m_fillStyle;
        Color m_fillColor;

        ToolPropertyEditor *m_propertyEditor;

        // InteractiveTool interface
    public:
        void mousePressEvent(QMouseEvent *event) override;
        void mouseMoveEvent(QMouseEvent *event) override;
        void mouseReleaseEvent(QMouseEvent *event) override;
        void mouseDoubleClickEvent(QMouseEvent *event) override;
        void keyPressEvent(QKeyEvent *event) override;
        void keyReleaseEvent(QKeyEvent *event) override;

        // Tool interface
    public:
        void activate() override;
        void desactivate() override;

    public slots:
        void applySettings(const Settings &settings) override;
        void setPalette(Palette palette) override;
    };

}
