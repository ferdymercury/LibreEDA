#pragma once

#include <QGraphicsPathItem>
#include "item/iobservableitem.h"
#include "OldGraphicsView/Palette.h"

namespace SymbolEditor
{

    class GraphicsItem;

    class Handle: public QGraphicsPathItem, public IObservableItem
    {
    public:
        enum { Type = UserType + 1976 };

        enum class Role
        {
            Move = 0,
            VSize,
            HSize,
            BDiagSize,
            FDiagSize,
            Rotate,
            Shear,
            Mark
        };

        enum class Shape
        {
            Circle = 0,
            Square,
            Diamond
        };

        explicit Handle(GraphicsItem *parent = nullptr);
        virtual ~Handle();

        QCursor handleCursor() const;

        virtual void setHandleId(int id);
        int handleId() const;
        void setHandleRole(Role role);
        Role handleRole() const;
        void setHandleShape(Shape shape);
        Shape handleShape() const;

        void setPalette(LeGraphicsView::Palette palette);
        LeGraphicsView::Palette palette() const;

    private:
        LeGraphicsView::Palette m_palette;
        static QCursor roleToCursor(Role role);
        int m_id;
        Role m_role;
        Shape m_handleShape;
        void updateShape();

        // QGraphicsItem interface
    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
        int type() const override { return Type; }
    };

}
