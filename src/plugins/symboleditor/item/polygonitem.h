#pragma once

#include "item/item.h"

namespace SymbolEditor
{

    class GraphicsPolygonItem : public GraphicsItem
    {
    public:
        enum { Type = ModelType + Polygon };

        explicit GraphicsPolygonItem(GraphicsItem *parent = nullptr);
        ~GraphicsPolygonItem();

        Qt::FillRule fillRule() const;
        void setFillRule(Qt::FillRule fillRule);
        QPolygonF polygon() const;
        void setPolygon(QPolygonF polygon);

        void addPoint(const QPointF &pos);
        void movePoint(int idx, const QPointF &pos);

    private:
        Qt::FillRule m_fillRule;
        QPolygonF m_polygon;
        void handleToPolygon();
        void polygonToHandle();
        QList<QLineF> toLines() const;

        // QGraphicsItem interface
    public:
        QRectF boundingRect() const override;
        QPainterPath shape() const override;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

        // QGraphicsItem interface
    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
        int type() const override { return Type; }

        // GraphicsItem interface
    public:
        GraphicsItem *clone() override;
        void setProperty(PropertyId id, const QVariant &value) override;
        QList<QPointF> endPoints() const override;
        QList<QPointF> midPoints() const override;
        QList<QPointF> centerPoints() const override;
        QList<QPointF> nearestPoints(QPointF pos) const override;

        // IGraphicsItemObserver interface
    public:
        void itemNotification(IObservableItem *item) override;

    };

}
