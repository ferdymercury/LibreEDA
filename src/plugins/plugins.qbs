import qbs

Project {
    name: "Plugins"
    references: [
        "filesystemnavigationview",
        "logviewer",
        "GerberViewer",
        "FootprintEditor",
        "symboleditor",
        "simpletexteditor",
    ].concat(project.additionalPlugins)
}
