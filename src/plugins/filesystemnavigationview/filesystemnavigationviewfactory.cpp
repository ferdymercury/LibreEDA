#include "filesystemnavigationviewfactory.h"
#include "filesystemnavigationwidget.h"

#include "core/core.h"

FileSystemNavigationViewFactory::FileSystemNavigationViewFactory(QObject *parent):
    INavigationViewFactory(parent)
{
    setDisplayName("File System");
}

FileSystemNavigationViewFactory::~FileSystemNavigationViewFactory()
{
}

NavigationView *FileSystemNavigationViewFactory::createView()
{
    NavigationView *view = new NavigationView();
    auto widget = new FileSystemNavigationWidget();
    widget->setCurrentDirectory(Core::dataPath("/samples"));
    view->widget = widget;
    return view;
}
