#ifndef LOGVIEWERPLUGIN_H
#define LOGVIEWERPLUGIN_H

#include "logviewer_global.h"
#include "core/extension/iplugin.h"

#include <QObject>

class LOGVIEWER_EXPORT LogViewerPlugin : public IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.libre-eda.leda.plugin" FILE "LogViewer.json")

public:
    explicit LogViewerPlugin(QObject *parent = nullptr);
    ~LogViewerPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    void shutdown();

private:
};

#endif // LOGVIEWERPLUGIN_H
