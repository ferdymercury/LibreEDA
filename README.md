# LibreEDA

[LibreEDA](https://www.libre-eda.org) aims to be a complete, modern and well integrated Open source [Electronics Design Automation](https://en.wikipedia.org/wiki/Electronic_design_automation).

It is written in `C++11` using the [Qt framework](https://qt.io).

## Status

LibreEDA is currently highly unstable and absolutely unusable - *We mean it*.

If you are looking for a usable Open Source EDA, check out [KiCad EDA](http://kicad-pcb.org/).

If you are a Qt/C++ developper, a Linux distro packager, a web designer, a system administrator, or an enthousiast hobyist then you are very welcome to join us.

## Download

Downloads of development snapshots are available for Linux, Mac OSX and Windows, see https://download.libre-eda.org/snapshots/master/latest/

*TODO: describe how to install Libre EDA on a per platform basis*

## Goals

### Milestone 1

1. Flexible but minimal application framework (WIP)
1. Symbol editor (WIP)
1. Footprint editor (POC)
1. Library manager (Not started)
1. Linux/MacOSX/Windows offline installers (WIP)

See [Milestone 1: LibreEDA-Librarians](https://gitlab.com/chgans/LibreEDA/milestones/1) for more details.

### Milestone 2

Too far away to bother.

## Concepts

### Multi platform

LibreEDA runs on modern 64 bits workstations with the following operating systems:
- Linux
- Mac OS X
- Windows

### Modern and efficient UI

- Configurability
- Ease of use with keyboard
- Uncluttered UI (Simple menus, limited tool bars, context-sensitive docks)
- Use of design patterns (Abstraction, Separation of concern, Modularity, ...)
- Extensibility (Simple main application + extension system + plugins)
- Technology (Qt, XML, JSON, Python)

### Document centric

- The document is the model
- GUI/Controller generates commands
- Commands are executed on a document
- GUI/View renders a document

### VHDL inspired

- Hierarchical design from ground up
- Entity/architecture/configuration approach

### Open source ecosystem integration

*TODO: Why? How?*

*TODO: FreeCAD (mechanical design), Qucs (electronic simulation), ...*

## Continuous integration

### Sanity checks

*TBD: Which tool? Which configuration?*

*TBD: cppcheck, clang satic analyser, ...*

### Build

| Builder               | Configuration                                           | Status  |
|-----------------------|---------------------------------------------------------|---------|
| Ubuntu 16.04, 64 bits | Qt 5.5, 5.6 and 5.7 for Linux 64 bits using gcc 5.3     | Active  |
| Ubuntu 16.04, 64 bits | Qt 5.5, 5.6 and 5.7 for Linux 64 bits using clang 3.8   | Active  |
| OSX 10.11, 64 bits    | Qt 5.5, 5.6 and 5.7 for Mac OSX 64 bits using Xcode 7.3 | Active  |
| Windows 7, 64 bits    | Qt 5.5, 5.6 and 5.7 for Windows 64 bits using MSVC 2013 | Active  |
| Windows 7, 64 bits    | Qt 5.5 and 5.6 for Windows 32 bits using Mingw 4.9      | Active  |
| Windows 7, 64 bits    | Qt 5.7 for Windows 32 bits using Mingw 5.3              | Active  |

### Unit tests

Every build configuration is unit tested before being packaged and deployed to the download area.

*TODO: improve unit testing*
*TBD: Code coverage?*

### Packaging

Standalone installer and binary archive are created for the folowing configurations

| Builder               | Configuration                                                   | Status      |
|-----------------------|-----------------------------------------------------------------|-------------|
| Ubuntu 16.04, 64 bits | Qt 5.7 for Linux 64 bits using gcc 5.3 and Qt Installer 2.0     | Active      |
| OSX 10.11, 64 bits    | Qt 5.7 for MacOSX 64 bits using XCode 7.3 and Qt Installer 2.0  | Active      |
| Windows 7, 64 bits    | Qt 5.7 for Windows 64 bits using MSVC 2013 and Qt Installer 2.0 | Active      |

### Acceptance tests

*TBD: How? Which configuration?*

*TBD: [FitNesse](http://www.fitnesse.org/) with [QtSlim](https://gitlab.com/chgans/QtSlim)?*
